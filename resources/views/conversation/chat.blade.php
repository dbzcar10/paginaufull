@extends('layout')
@push('chat_css')
<link rel="stylesheet" href="{{asset('css/chat.css')}}">
@endpush

@section('content')
<div class="row chat-row">

    <div class="col-md panel-conversaciones">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-chat">Usuarios</div>

                    <div class="panel-body">
                        @foreach($users as $user)
                            <div class="row">
                            <div class="col-md-6">
                            {{$user->name}}
                            </div>
                            <div class="col-md-6">
                            {{Form::open(['url'=>route('conversation.store')])}}
                            {{Form::hidden('user_id',$user->id)}}
                            {{Form::submit('add',['class'=>'form-control'])}}
                            {{Form::close()}}
                            </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-chat">Conversaciones</div>

                    <div class="panel-body">
                        @foreach($conversations as $conversation)
                            <a href="{{route('conversation.show',$conversation->id)}}">
                            {{($conversation->user1()->first()->id==Auth::user()->id)?$conversation->user2()->first()->name:$conversation->user1()->first()->name}}
                            </a>
                            <hr/>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contenedor col-md">

        <div class="col-md-offset-4 col-md-12 chat-movil">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">

                        <a data-toggle="collapse" href="#chatCollapse" role="button" aria-expanded="true" aria-controls="chatCollapse">
                        <p class="text-chat">Estas hablando con: {{($conversationp->user1()->first()->id==Auth::user()->id)?$conversationp->user2()->first()->name:$conversationp->user1()->first()->name}}</p>
                        </a>
                            
                        </div>
                    </div>
                </div>

            <div class="collapse" id="chatCollapse">
                    <div class="panel-body" id="panel-body">
                    @foreach($conversationp->messages as $message)
                        @if($message->user_id!=Auth::user()->id)
                        <div class="row row-not-owner">
                            <div class="message {{ ($message->user_id!=Auth::user()->id)?'not_owner':'owner'}}">
                                {{$message->text}}<br/>
                                <b>{{$message->created_at->diffForHumans()}}</b>
                            </div>
                        </div>
                        @else
                        <div class="row row-owner">
                            <div class="message {{ ($message->user_id!=Auth::user()->id)?'not_owner':'owner'}}">
                                {{$message->text}}<br/>
                                <b>{{$message->created_at->diffForHumans()}}</b>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
                <div class="panel-footer">
                        <textarea id="msg" class="form-control col-8" placeholder="Escribe tu mensaje" rows="1"></textarea>
                        <input type="hidden" id="csrf_token_input" value="{{csrf_token()}}"/>
                        <br/>
                            <div class="col-md-offset-4 col-md-4">
                            <button class="btn btn-primary btn-block" onclick="button_send_msg()">Enviar</button>
                            </div>
                </div>
            </div>
                
            </div>
        </div>
    </div>
</div>



@endsection

@push('chat_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    <script>
        var socket = io.connect('http://127.0.0.1:8890');
        socket.emit('add user', {'client':{{Auth::user()->id}},'conversation':{{$conversationp->id}} });

        socket.on('message', function (data) {
            $('#panel-body').append(
                    '<div class="row row-not-owner">'+
                    '<div class="message not_owner">'+
                    data.msg+'<br/>'+
                    '<b>now</b>'+
                    '</div>'+
                    '</div>');

            scrollToEnd();

         });
    </script>
    <script>
        $(document).ready(function(){
            scrollToEnd();

            $(document).keypress(function(e) {
                if(e.which == 13) {
                    var msg = $('#msg').val();
                    $('#msg').val('');//reset
                    send_msg(msg);
                }
            });
        });

        function button_send_msg(){
            var msg = $('#msg').val();
            $('#msg').val('');//reset
            send_msg(msg);
        }


        function send_msg(msg){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('#csrf_token_input').val() },
                type: "POST",
                url: "{{route('message.store')}}",
                data: {
                    'text': msg,
                    'conversation_id':{{$conversationp->id}},
                },
                success: function (data) {
                    if(data==true){

                        $('#panel-body').append(
                                '<div class="row row-owner">'+
                                '<div class="message owner">'+
                                msg+'<br/>'+
                                '<b>ora</b>'+
                                '</div>'+
                                '</div>');

                        scrollToEnd();
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function scrollToEnd(){
            var d = $('#panel-body');
            d.scrollTop(d.prop("scrollHeight"));
        }


$('.hide-chat-box').click(function(){
$('.chat-content').slideToggle();
});
    </script>
@endpush
