
@extends('layout')
<!--CAROUSEL INICIO-->
@push('chat_css')
<link rel="stylesheet" href="{{asset('css/chat.css')}}">
@endpush
<div class="row justify-content-center">
    <div class="col-sm-0"></div>
</div>


@section('content')
	<section class="posts container">
	{{-- 	
			@if (isset ($titulo))
			<h3>{{ $titulo }}</h3>
			@endif


		@foreach($publicaciones as $publicacion)

		<article class="post">

			@include( $publicacion->viewType('home'))

			<div class="content-post">
				
				

				@include('publicaciones.header')

			
				<h1>{{$publicacion->titulo}}</h1>
				<div class="divider"></div>
				<p>{{$publicacion->extracto}}</p>
				
				<footer class="container-flex space-between">
					<div class="read-more">
						<a href="{{ route('publicaciones.show', $publicacion) }}" class="text-uppercase c-green">Leer mas</a>					</div>
					</div>

				</footer>
			</div>
		</article>

		@endforeach   --}}

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-chat">Usuarios</div>

                <div class="panel-body">
                    @foreach($users as $user)
                        <div class="row">
                        <div class="col-md-6">
                        {{$user->name}}
                        </div>
                        <div class="col-md-6">
                        {{Form::open(['url'=>route('conversation.store')])}}
                        {{Form::hidden('user_id',$user->id)}}
                        {{Form::submit('add',['class'=>'form-control'])}}
                        {{Form::close()}}
                        </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-chat">Conversaciones</div>

                <div class="panel-body">
                    @foreach($conversations as $conversation)
                        <a href="{{route('conversation.show',$conversation->id)}}">
                        {{($conversation->user1()->first()->id==Auth::user()->id)?$conversation->user2()->first()->name:$conversation->user1()->first()->name}}
                        </a>
                        <hr/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

	</section><!-- fin del div.posts.container -->

	<div class="container">
		{{--{{ $publicaciones->links('vendor/pagination/default') }}--}}
	</div>
	
    
@stop
