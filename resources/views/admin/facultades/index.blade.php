@extends('admin.layoutadmin')

@section('header')
<section class="content-header">
    <h1>
      Listado de Facultades
      <small>Todas las facultades</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Facultades</li>
    </ol>
  </section>

  @endsection

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Listado de Facultades</h3>

      {{--<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#facultadModal">
        <i class="fa fa-plus"></i>Agregar Facultad</button>--}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="facultades-table" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
            </thead>

            <tbody>
                    @foreach($facultades as $facultad)
      
                    <tr>
                        <td>{{$facultad->id}}</td>
                        <td>{{$facultad->nombre}}</td>
                        <td>
                        {{--<a href="#"
                            class="btn btn-default btn-sm"
                            target="_blank"
                         ><i class="fa fa-eye"></i></a>--}}

                        <a href="{{ route('admin.facultades.edit', $facultad) }}"
                            class="btn btn-info btn-sm"
                            ><i class="fa fa-pencil"></i></a>

                        {{--<form method="POST" 
                        action="{{ route('admin.facultades.destroy', $facultad) }}"
                        style="display: inline">
                        {{ csrf_field() }} {{ method_field('DELETE') }}

                        <button class="btn btn-danger btn-sm"
                          onclick="return confirm('Estas seguro de querer eliminar esta facultad?')"
                        ><i class="fa fa-times"></i></button>

                        </form>--}}

                        </td>
                    </tr>
      
                    @endforeach
      
            </tbody>
            
          </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection


@push('styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- page script -->
<script>
    $(function () {
      $('#facultades-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,

        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
            "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",           
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "search": "Buscar:",
              "paginate": {
              "first":      "Primer",
              "last":       "Ultimo",
              "next":       "Siguiente",
              "previous":   "Anterior"
        }
        }
      });
    });
  </script>

  @endpush