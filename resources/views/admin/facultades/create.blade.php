<!-- Modal -->
<div class="modal fade" id="facultadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form method="POST" action="{{route('admin.facultades.store','#createFacultad') }}">
        {{csrf_field()}}

  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Nombre de tu nueva facultad</h4>
        </div>
        <div class="modal-body">
          
            <div class="form-group {{ $errors->has('nombre') ? 'has-error': '' }}">
                <!--<label>nombre de la facultad</label>-->
                <input class="form-control" id="facultad-nombre" name="nombre"
                value="{{old('nombre')}}"
                placeholder="Ingresa nombre de la facultad" autofocus required type="nombre">
                
                {!! $errors->first('nombre', '<span class="help-block">:message</span>') !!}
                
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary">Crear facultad</button>
        </div>
      </div>
    </div>
    </form>
  </div>

  @push('scripts')
   <script>

      if(window.location.hash === '#createFacultad')
       {
         $('#facultadModal').modal('show');
       }
    
       $('#facultadModal').on('hide.bs.modal', function(){
          window.location.hash = '#';
    
       });
    
       $('#facultadModal').on('shown.bs.modal', function(){
         $('#facultad-nombre').focus();
          window.location.hash = '#createFacultad';
    
       }); 
    </script>
  @endpush