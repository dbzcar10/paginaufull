@extends('admin.layoutadmin')

@section('header')
    <section class="content-header">
        <h1>
          CATEDRATICOS
          <small>Agregar Catedratico</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
          <li><a href="{{route('admin.catedraticos.index')}}"><i class="fa fa-list"></i> Catedraticos</a></li>
          <li class="active">Agregar</li>
        </ol>
    </section>
@stop

@section('content')

<div class="row">

    <form method="POST" action="{{route('admin.catedraticos.update', $catedratico)}}">
        {{csrf_field()}} {{ method_field('PUT') }}
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('titulo') ? 'has-error': '' }}">
                            <label>Correo catedratico</label>
                                    <input class="form-control" placeholder="Ingresa Correo" name="email"
                                    value="{{old('email', $catedratico->email)}}">
                                    
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    
                        </div>

                    </div>

                </div>                
            </div>

        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">
        
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Guardar catedratico</button>
                    </div>

                </div>

            </div>
        </div>

    </form>

</div>               


@stop
