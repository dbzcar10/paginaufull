<!-- Modal -->
<div class="modal fade" id="catedraticoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form method="POST" action="{{route('admin.catedraticos.store','#createCatedratico') }}">
        {{csrf_field()}}

  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Correo de tu nuevo catedratico</h4>
        </div>
        <div class="modal-body">
          
            <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                <!--<label>email de los catedratico</label>-->
                <input class="form-control" id="catedratico-email" name="email"
                value="{{old('email')}}"
                placeholder="Ingresa email de tu catedratico" autofocus required type="email">
                
                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary">Crear catedratico</button>
        </div>
      </div>
    </div>
    </form>
  </div>

  @push('scripts')
   <script>

      if(window.location.hash === '#createCatedratico')
       {
         $('#catedraticoModal').modal('show');
       }
    
       $('#catedraticoModal').on('hide.bs.modal', function(){
          window.location.hash = '#';
    
       });
    
       $('#catedraticoModal').on('shown.bs.modal', function(){
         $('#catedratico-email').focus();
          window.location.hash = '#createCatedratico';
    
       }); 
    </script>
  @endpush