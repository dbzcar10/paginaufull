@extends('admin.layoutadmin')

@section('content')
    <div class="row">
       <div class="col-md-6">
           <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Datos personales</h3>
                </div>

                <div class="box-body">
                    @if ($errors->any())

                        <ul class="list-group">
                            @foreach ($errors->all() as $error)
                                <li class="list-group-item list-group-item-danger">
                                    {{$error}}
                                </li>                                
                            @endforeach
                        </ul>
                        
                    @endif
                    <form method="POST" action="{{route('admin.users.update', $user) }}">
                    {{csrf_field()}} {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Nombre:</label>
                            <input name="name" value="{{ old('name', $user->name)}}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input name="email" value="{{ old('email', $user->email)}}" class="form-control" type="email">
                        </div>

                        <div class="form-group">
                            <label>Facultades</label>
                                <select class="form-control" name="facultad_id">
                                    <option value="">Selecciona Facultad</option>
                                        @foreach($facultades as $facultad)
                                            <option value="{{ $facultad->id }}"
                                            {{old('facultad_id', $user->facultad_id ) == $facultad->id ? 'selected': ''}}
                                            >{{ $facultad->nombre }}</option>

                                        @endforeach
                                </select>
                        </div>

                        @role('Admin')

                        <div class="form-group">
                            <label>Estado</label>
                                <select class="form-control" name="estado">
                                    <option value="">Selecciona estado</option>
                                    <option value="1" {{old('estado', $user->estado ) == '1' ? 'selected': ''}}> Activo </option>
                                    <option value="0" {{old('estado', $user->estado ) == '0' ? 'selected': ''}}> Inactivo </option>
                                </select>
                        </div>

                        @endrole

                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Actualizar Usuario</button>
                        </div>

                        
                    </form>
                </div>
           </div>
       </div>

       <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>Roles</h3>
                </div>

                <div class="box-body">

                    @role('Admin')

                    <form method="POST" action="{{ route('admin.users.roles.update', $user)}}">
                    {{ csrf_field() }} {{method_field('PUT')}}
                         @foreach ($roles as $role)
                            <div class="checkbox">
                                <label>
                                    <input name="roles[]" type="checkbox" value="{{$role->name}}" 
                                    {{$user->roles->contains($role->id) ? 'checked': ''}}>
                                    {{ $role->name}} <br>
                                    <small class="text-muted">{{$role->permissions->pluck('name')->implode(', ')}}</small>
                                </label>
                            </div>
                        @endforeach

                        <button class="btn btn-primary btn-block">Actualizar roles</button>
                    </form>
                    @else
                        <ul class="list-group">
                            @forelse ($user->roles as $role)
                                <li class="list-group-item">{{ $role->name}}</li>
                            @empty
                                <li class="list-group-item">No tiene roles</li>                                 
                            @endforelse
                        </ul>
                    @endrole
                   
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>Permisos</h3>
                </div>

                <div class="box-body">

                    @role('Admin')
                    <form method="POST" action="{{ route('admin.users.permissions.update', $user)}}">
                    {{ csrf_field() }} {{method_field('PUT')}}
                         @foreach ($permissions as $id =>$name)
                            <div class="checkbox">
                                <label>
                                    <input name="permissions[]" type="checkbox" value="{{$name}}" 
                                    {{$user->permissions->contains($id) ? 'checked': ''}}>
                                    {{ $name}}
                                </label>
                            </div>
                        @endforeach

                        <button class="btn btn-primary btn-block">Actualizar Permisos</button>
                    </form>
                   @else
                        <ul class="list-group">
                            @forelse ($user->permissions as $permission)
                                <li class="list-group-item">{{ $permission->name}}</li>
                            @empty
                                <li class="list-group-item">No tiene permisos asignados</li>

                            @endforelse
                        </ul>
                    @endrole
                   
                </div>
            </div>
       </div> 
    </div>
    
@endsection
