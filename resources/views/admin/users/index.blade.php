@extends('admin.layoutadmin')

@section('header')
<section class="content-header">
    <h1>
      Listado de Usuarios
      <small>Todas los usuarios</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Usuarios</li>
    </ol>
  </section>

  @endsection

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Listado de Usuarios</h3>

      {{--@can('create', new App\User)

      <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#userModal">
        <i class="fa fa-plus"></i>Agregar Usuario</button>
      @endcan --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="users-table" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Estado</th>
              <th>Facultad</th>
              <th>Roles</th>
              <th>Acciones</th>
            </tr>
            </thead>

            <tbody>
                    @foreach($users as $user)
      
                      <tr>
                          <td>{{$user->id}}</td>
                          <td>{{$user->name}}</td>
                           @if($user->estado == 1 )
                          <td>Activo</td>

                          @else
                          <td>Inactivo</td>
                          @endif
                          <td>{{$user->facultad->nombre }}</td>
                          <td>{{$user->getRoleNames()->implode(', ') }}</td>
                          
                          <td>
                          <a href="{{route('admin.users.show', $user)}}"
                              class="btn btn-default btn-sm"
                          ><i class="fa fa-eye"></i></a>

                          @can('update', $user)

                          <a href="{{ route('admin.users.edit', $user) }}"
                              class="btn btn-info btn-sm"
                              ><i class="fa fa-pencil"></i></a>

                        @endcan

                          {{--@can('delete', $user)
                          <form method="POST" 
                          action="{{ route('admin.users.destroy', $user) }}"
                          style="display: inline">
                          {{ csrf_field() }} {{ method_field('DELETE') }}

                          <button class="btn btn-danger btn-sm"
                            onclick="return confirm('Estas seguro de querer eliminar este usuario?')"
                          ><i class="fa fa-times"></i></button>

                          </form>

                          @endcan--}}

                          </td>
                      </tr>
      
                    @endforeach
      
            </tbody>
            
          </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection


@push('styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
 
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- page script -->
<script>
    $(function () {
      $('#users-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,

        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
            "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",           
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "search": "Buscar:",
              "paginate": {
              "first":      "Primer",
              "last":       "Ultimo",
              "next":       "Siguiente",
              "previous":   "Anterior"
        }
        }
      });
    });

  </script>

  @endpush