@extends('admin.layoutadmin')

@section('content')

    <div class="row">
        <div class="col-md-3">
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{ auth()->user()->url }}" alt="{{$user->name}}">

              <h3 class="profile-username text-center">{{$user->name}}</h3>

              <p class="text-muted text-center">{{$user->getRoleNames()->implode(', ')}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Email</b> <a class="pull-right">{{$user->email}}</a>
                </li>
                <li class="list-group-item">
                  <b>Publicaciones</b> <a class="pull-right">{{$user->publicaciones->count() }}</a>
                </li>
                @if ($user->roles->count())

                   <li class="list-group-item">
                      <b>Roles</b> <a class="pull-right">{{$user->getRoleNames()->implode(', ')}}</a>
                   </li>

                @endif


                <li class="list-group-item">
                  <b>Facultad</b> <a class="pull-right">{{$user->facultad->nombre}}</a>
                </li>
               
              </ul>

              @can('update',$user)
              <a href="{{route('admin.users.edit', $user)}}" class="btn btn-primary btn-block"><b>Editar</b></a>
              @endcan

            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-md-3">
          <div class="box prymary">
            <div class="box-header with-border">
              <h3 class="box-title">Publicaciones</h3>
            </div>

            <div class="box-body">
              @forelse ($user->publicaciones as $publicacion)

                <strong>{{ $publicacion->titulo}}</strong><br>
                <small class="text-muted">Publicado el {{ $publicacion->publicado_el->format('d-m-y')}}</small>
                <p class="text-muted">{{$publicacion->extracto}}</p>
                @unless($loop->last)
                  <hr>
                @endunless

              @empty
                <small class="test-muted">No tiene ninguna publicacion</small>

              @endforelse
              
            </div>
          
                    
          </div>
        </div>

        <div class="col-md-3">
          <div class="box prymary">
            <div class="box-header with-border">
              <h3 class="box-title">Roles</h3>
            </div>

            <div class="box-body">
              @forelse ($user->roles as $role)

                <strong>{{ $role->name}}</strong>

                @if( $role->permissions->count())
                  <br>
                  <small class="text-muted">
                    Permisos: {{$role->permissions->pluck('name')->implode(', ')}}
                  </small>

                @endif
                
                @unless($loop->last)
                  <hr>
                @endunless

              @empty
                <small class="text-muted">No tiene ningun rol asociado</small>

              @endforelse
              
            </div>
          
                    
          </div>
        </div>

        <div class="col-md-3">
          <div class="box prymary">
            <div class="box-header with-border">
              <h3 class="box-title">Permisos adicionales</h3>
            </div>

            <div class="box-body">
              @forelse ($user->permissions as $permission)

                <strong>{{ $permission->name}} </strong><br>
                @unless($loop->last)
                  <hr>
                @endunless
              @empty

              <small class="text-muted">No tiene permisos adicionales</small>

              @endforelse
              
            </div>
          
                    
          </div>
        </div>
    </div>

@endsection
