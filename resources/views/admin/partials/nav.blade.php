    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li class="header">Navegacion</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="{{ setActiveRoute('dashboard')}}"><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> <span>Inicio</span></a></li>

        {{--<li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>--}}
        <li class="treeview {{setActiveRoute(['admin.publicaciones.index', 'admin.publicaciones.edit'])}}">
          <a href="#"><i class="fa fa-bars"></i> <span>Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ setActiveRoute('admin.publicaciones.index') }}">
              <a href="{{route('admin.publicaciones.index')}}"><i class="fa fa-eye"></i>Ver todas las Publicaciones</a>
            </li>

            @can('create', new App\Publicacion)

            <li>
              @if(request()->is('admin/publicaciones/*'))
                <a href="{{ route('admin.publicaciones.index', '#create') }}" ><i class="fa fa-pencil"></i>Crear Publicacion</a>                
              
              @else
                <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i>Crear Publicacion</a>
              @endif                
              
            </li>
            @endcan

          </ul>

        </li>


        @can('view', new App\Catedratico)
        <li class="treeview {{setActiveRoute(['admin.catedraticos.index', 'admin.catedraticos.edit'])}}">
          <a href="#"><i class="fa fa-user"></i> <span>Catedraticos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{setActiveRoute('admin.catedraticos.index')}}"><a href="{{route('admin.catedraticos.index')}}"> 
              <i class="fa fa-eye"></i>Ver todos los catedraticos</a>
            </li>

            <li>
              @if(request()->is('admin/catedraticos/*'))
                <a href="{{ route('admin.catedraticos.index', '#createCatedratico') }}" ><i class="fa fa-pencil"></i>Agregar Catedratico</a>                
              
              @else
                <a href="#" data-toggle="modal" data-target="#catedraticoModal"><i class="fa fa-pencil"></i>Agregar Catedratico</a>
              @endif                
              
            </li>

          </ul>
          
        </li>
        @endcan

        @can('view', new App\PersonalAdministrativo)
        <li class="treeview {{setActiveRoute(['admin.personales_administrativos.index', 'admin.personales_administrativos.edit'])}}">
          <a href="#"><i class="fa fa-user"></i> <span>Personal Administrativo</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{setActiveRoute('admin.personales_administrativos.index')}}"><a href="{{route('admin.personales_administrativos.index')}}"> 
              <i class="fa fa-eye"></i>Ver todos</a>
            </li>

            <li>
              @if(request()->is('admin/personales_administrativos/*'))
                <a href="{{ route('admin.personales_administrativos.index', '#createPersonal') }}" ><i class="fa fa-pencil"></i>Agregar personal administrativo</a>                
              
              @else
                <a href="#" data-toggle="modal" data-target="#personalModal"><i class="fa fa-pencil"></i>Agregar personal administrativo</a>
              @endif                
              
            </li>

          </ul>
          
        </li>
        @endcan
        

        @can('view', new App\Facultad)
        <li class="treeview {{setActiveRoute(['admin.facultades.index', 'admin.facultades.edit'])}}">
          <a href="#"><i class="fa fa-university"></i> <span>Facultades</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{setActiveRoute('admin.facultades.index')}}"><a href="{{route('admin.facultades.index')}}"> 
              <i class="fa fa-eye"></i>Ver todas las facultades</a>
            </li>

            {{--

            @can('create', new App\Facultad)

            <li>
              @if(request()->is('admin/facultades/*'))
                <a href="{{ route('admin.facultades.index', '#createFacultad') }}" ><i class="fa fa-pencil"></i>Agregar Facultad</a>                
              
              @else
                <a href="#" data-toggle="modal" data-target="#facultadModal"><i class="fa fa-pencil"></i>Agregar Facultad</a>
              @endif                
              
            </li>
            @endcan--}}

          </ul>
          
        </li>
        @endcan

        <li class="treeview {{setActiveRoute(['admin.users.index', 'admin.users.edit'])}}">
          <a href="#"><i class="fa fa-users"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{setActiveRoute('admin.users.index')}}"><a href="{{route('admin.users.index')}}"> 
              <i class="fa fa-eye"></i>Ver todos los usuarios</a>
            </li>

            {{--@can('create', new App\User)

            <li>
                <a href="{{ route('admin.users.create') }}" ><i class="fa fa-pencil"></i>Agregar Usuario</a>                
                              
            </li>
            @endcan --}}

          </ul>
          
        </li>

        @can('Mantenimiento')
        <li class="treeview">
          <a href="#"><i class="fa fa-building"></i> <span>Mantenimiento Pagina</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(file_exists(storage_path()."/"."framework/down"))

            <li>
              <a style="color:white" class="btn btn-success" href="{{route('admin.mantenimiento')}}">Desactivar<i class="fa fa-arrow-up"></i></a>
            </li>

            @else

            <li>
                <a style="color:white" class="btn btn-danger" href="{{route('admin.mantenimiento')}}">Activar<i class="fa fa-arrow-down"></i></a>                
                              
            </li>

            @endif
          </ul>   
        </li>
        
        @endcan
          
    </ul>

    <!-- /.sidebar-menu -->