@extends('admin.layoutadmin')

@section('header')
    <section class="content-header">
        <h1>
          POSTS
          <small>Crear Publicacion</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
          <li><a href="{{route('admin.publicaciones.index')}}"><i class="fa fa-list"></i> Posts</a></li>
          <li class="active">Crear</li>
        </ol>
    </section>
@stop

@section('content')

<div class="row">

    @if ($publicacion->archivos->count())
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
            
                    @foreach($publicacion->archivos as $archivo)
                        <form  method="POST" action=" {{ route('admin.archivos.destroy', $archivo) }}">
                            {{ method_field('DELETE') }} {{ csrf_field() }}   
                                <div class="col-md-2">

                                    @if(str_contains($archivo->url, '.jpg') || str_contains($archivo->url, '.png') || 
                                        str_contains($archivo->url, '.gif') || str_contains($archivo->url, '.svg') ||
                                        str_contains($archivo->url, '.bmp') || str_contains($archivo->url, '.jpeg')||
                                        str_contains($archivo->url, 'tiff'))

                                        <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                        <a href="{{ route('archivos.show', $archivo) }}" target="_blank"> <img src="{{ url($archivo->url) }}" class="img-responsive"> </a>
                                        <p>{{$archivo->nombre}}</p>
                                        <a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary" style="display:block">Descargar <i class="fa fa-arrow-down"></i></a>
                                    
                                    @elseif(str_contains($archivo->url, '.pdf'))

                                        <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                        <a href="{{ route('archivos.show', $archivo) }}" target="_blank"> <img src="{{asset('adminlte/img/pdf.jpg')}}" class="img-responsive"> </a>
                                        <p>{{$archivo->nombre}}</p> 
                                        <a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary" style="display:block">Descargar <i class="fa fa-arrow-down"></i></a>

                                    @elseif(str_contains($archivo->url, '.xls') || str_contains($archivo->url, '.xlsx') )

                                        <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                        <a href="{{ route('archivos.show', $archivo) }}" target="_blank"> <img src="{{asset('adminlte/img/excel.svg')}}" class="img-responsive" style="width:65%"> </a>
                                        <p>{{$archivo->nombre}}</p> 
                                        <a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary" style="display:block">Descargar <i class="fa fa-arrow-down"></i></a>
                                               

                                    @elseif(str_contains($archivo->url, '.doc') || str_contains($archivo->url, '.docx') )

                                        <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                        <a href="{{ route('archivos.show', $archivo) }}" target="_blank"> <img src="{{asset('adminlte/img/word.svg')}}" class="img-responsive"> </a>
                                        <p>{{$archivo->nombre}}</p> 
                                        <a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary" style="display:block">Descargar <i class="fa fa-arrow-down"></i></a>

                                    @elseif(str_contains($archivo->url, '.zip') || str_contains($archivo->url, '.rar') )

                                        <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                        <a href="{{ route('archivos.show', $archivo) }}" target="_blank"> <img src="{{asset('adminlte/img/rar.svg')}}" class="img-responsive" style="width:65%"> </a>
                                        <p>{{$archivo->nombre}}</p> 
                                        <a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary" style="display:block">Descargar <i class="fa fa-arrow-down"></i></a>
                                               
                                    @else
                                        <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                        <a href="{{ route('archivos.show', $archivo) }}" target="_blank"> <img src="{{asset('adminlte/img/archivo.svg')}}" class="img-responsive" style="width:65%"> </a>
                                        <p>{{$archivo->nombre}}</p> 
                                        <a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary" style="display:block">Descargar <i class="fa fa-arrow-down"></i></a>                                     
                                            
                                    @endif

                                </div>
    
                        </form>
                     
                    @endforeach
            
                </div>
            </div>
        </div>

    @endif


    <form method="POST" action="{{route('admin.publicaciones.update', $publicacion)}}">
        {{csrf_field()}} {{ method_field('PUT') }}
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('titulo') ? 'has-error': '' }}">
                            <label>Titulo de la Publicacion</label>
                                    <input class="form-control" placeholder="Ingresa Titulo" name="titulo"
                                    value="{{old('titulo', $publicacion->titulo)}}">
                                    
                                    {!! $errors->first('titulo', '<span class="help-block">:message</span>') !!}
                                    
                        </div>
                                      
                        <div class="form-group {{ $errors->has('cuerpo') ? 'has-error': '' }}">
                            <label>Contenido de la Publicacion</label>
                            <textarea id="editor" name="cuerpo" class="form-control" placeholder="Ingresa contenido completo de la publicacion" rows="10">{{old('cuerpo', $publicacion->cuerpo)}}</textarea>
                            {!! $errors->first('cuerpo', '<span class="help-block">:message</span>') !!}
                        </div>

                    </div>

                </div>                
            </div>

        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        <label>Fecha de Publicacion:</label>
                            
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                                <input name="publicado_el" type="text" class="form-control pull-right" id="datepicker" value="{{old('publicado_el', $publicacion->publicado_el ? $publicacion->publicado_el->format('m/d/Y') : null) }}">
                            </div>
                                    <!-- /.input group -->
                    </div>
                                          
                    <div class="form-group {{ $errors->has('categoria_id') ? 'has-error': '' }}">
                        <label>Categorias</label>
                            <select class="form-control select2" name="categoria_id">
                                <option value="">Selecciona una categoria</option>
                                    @foreach($categorias as $categoria)
                                        <option value="{{ $categoria->id }}"
                                        {{old('categoria_id', $publicacion->categoria_id ) == $categoria->id ? 'selected': ''}}
                                        >{{ $categoria->nombre }}</option>

                                    @endforeach
                            </select>

                            {!! $errors->first('categoria_id', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('facultad_id') ? 'has-error': '' }}">
                        <label>Facultades</label>
                            <select class="form-control" name="facultad_id" id="facultadid">
                                <option value="">Selecciona Facultad</option>
                                    @foreach($facultades as $facultad)
                                        <option value="{{ $facultad->id }}"
                                        {{old('facultad_id', $publicacion->facultad_id ) == $facultad->id ? 'selected': ''}}
                                        >{{ $facultad->nombre }}</option>

                                    @endforeach
                            </select>

                            {!! $errors->first('facultad_id', '<span class="help-block">:message</span>') !!}
                    </div>



                     <div class="form-group {{ $errors->has('segu_externa') ? 'has-error': '' }}">
                        <label>Visualización Externa</label>
                            <select class="form-control" name="segu_externa" id='segu_externa'>
                                <option value="">Selecciona</option>
                                
                                <option value="Privada" 
                                {{old('segu_externa', $publicacion->segu_externa ) == 'Privada' ? 'selected': ''}}
                                >Privada</option>

                                @can('Crear publica externa')
                                <option value="Publica" 
                                {{old('segu_externa', $publicacion->segu_externa ) == 'Publica' ? 'selected': ''}}
                                >Publica</option>
                                @endcan

                            </select>

                            {!! $errors->first('segu_externa', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <div class="form-group {{ $errors->has('segu_interna') ? 'has-error': '' }}">
                        <label>Visualización Interna</label>
                            <select class="form-control" name="segu_interna" id="segu_interna">
                                <option value="">Selecciona seguridad</option>
                                
                              
                                <option value="Privada" 
                                {{old('segu_interna', $publicacion->segu_interna ) == 'Privada' ? 'selected': ''}}
                                >Privada</option>

                                <option value="Publica" 
                                {{old('segu_interna', $publicacion->segu_interna ) == 'Publica' ? 'selected': ''}}
                                >Publica</option>                             
                            </select>

                            {!! $errors->first('segu_interna', '<span class="help-block">:message</span>') !!}
                    </div>


                    <div class="form-group {{ $errors->has('extracto') ? 'has-error': '' }}">
                        <label>Extracto</label>
                            <textarea name="extracto" class="form-control" placeholder="Ingresa un extracto de la publicacion">{{ old('extracto', $publicacion->extracto)}}</textarea>
                                {!! $errors->first('extracto', '<span class="help-block">:message</span>') !!}
                    </div>
        
                    <div class="form-group">
                        <div class="dropzone"></div>
                    </div>
        
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Guardar Publicacion</button>
                    </div>

                </div>

            </div>
        </div>

    </form>

</div>               


@stop


@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/plugins/datepicker/datepicker3.css')}}">

<!-- Select2 -->
<link rel="stylesheet" href="{{asset('adminlte/plugins/select2/select2.min.css')}}">

@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.js"></script>

<!-- bootstrap datepicker -->
<script src="{{asset('adminlte/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

<!-- Select2 -->
<script src="{{asset('adminlte/plugins/select2/select2.full.min.js')}}"></script>

<script>
//Date picker
$('#datepicker').datepicker({
  autoclose: true
});
$('.select2').select2({
    tags: true
});

CKEDITOR.replace('editor');
CKEDITOR.config.height = 316;

var myDropzone = new Dropzone('.dropzone',{
    //url:'/admin/publicaciones/{{ $publicacion->url }}/archivos',
    url: "{{route('admin.publicaciones.archivos.store', $publicacion->url)}}",
    paramName: 'archivo',
    //acceptedFiles: 'image/*', //para aceptar solo imagenes
    maxFilesize: 20,
    //maxFiles: 14,
    headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    dictDefaultMessage: 'Arrastra los archivos aqui para subirlos'
});

myDropzone.on('error', function(file, res){
    var msg = res.errors.archivo[0];
    $('.dz-error-message:last > span').text(msg);

});

Dropzone.autoDiscover = false;


$(document).on('change', '#segu_externa', function(event) {
    var externo = $("#segu_externa option:selected").text();

        if(externo == 'Publica'){

            $("#segu_interna option[value='Privada']").hide();
            
        }

        else{
            $("#segu_interna option[value='Privada']").show();
        }      
    
});

/*$(document).ready(function() {
var valor = $("#facultadid option:selected").text();
    if(valor == 'Sin asignar'){
        $("#segu_externa option[value='Privada']").hide();
    }
});

$(document).on('change', '#facultadid', function(event) {
    var sinasignar = $("#facultadid option:selected").text();

        if(sinasignar == 'Sin asignar'){

            $("#segu_externa option[value='Privada']").hide();
            
        }    
    
});*/

</script>

@endpush