@extends('admin.layoutadmin')

@section('header')
<section class="content-header">
    <h1>
      Todas las publicaciones
      <small>Todas las Publicaciones</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Publicaciones</li>
    </ol>
  </section>

  @endsection

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Listado de Publicaciones</h3>

      @can('create', new App\Publicacion)

      <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">
        <i class="fa fa-plus"></i>Crear publicacion</button>
      @endcan
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="posts-table" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Titulo</th>
              <th>Extracto</th>
              <th>Estado</th>
              <th>Facultad</th>
              <th>Acciones</th>
            </tr>
            </thead>

            <tbody>
                    @foreach($publicaciones as $publicacion)
      
                    <tr>
                        <td>{{$publicacion->id}}</td>
                        <td>{{$publicacion->titulo}}</td>
                        <td>{{$publicacion->extracto}}</td>
                        @if($publicacion->estado == 1)
                        <td>Aciva</td>
                        @else
                        <td>Inactiva</td>
                        @endif
                        <td>

                        @if ($publicacion->facultad) 
                        {{$publicacion->facultad->nombre }}
                        @endif

                        </td>
                        <td>
                        <a href="{{ route('publicaciones.show', $publicacion) }}"
                            class="btn btn-default btn-sm"
                            target="_blank"
                            data-toggle="tooltip" data-placement="top" title="Ver"
                         ><i class="fa fa-eye"></i></a>

                        @can('update', $publicacion)
                        <a href="{{ route('admin.publicaciones.edit', $publicacion) }}"
                          class="btn btn-info btn-sm"
                          data-toggle="tooltip" data-placement="top" title="Editar"
                          ><i class="fa fa-pencil"></i></a>
                        @endcan

                        @can('delete', $publicacion)
                        @if($publicacion->estado == 1)
                        <form method="POST" 
                        action="{{ route('admin.publicaciones.destroy', $publicacion) }}"
                        style="display: inline">
                        {{ csrf_field() }} {{ method_field('DELETE') }}

                        <button class="btn btn-danger btn-sm"
                          data-toggle="tooltip" data-placement="top" title="Desactivar"
                          onclick="return confirm('Estas seguro de querer desactivar esta publicacion?')"
                        ><i class="fa fa-times"></i></button>
                        </form>
                        @endif

                        @if($publicacion->estado == 0)
                        <form method="POST" 
                        action="{{ route('admin.publicaciones.activar', $publicacion) }}"
                        style="display: inline">
                        {{ csrf_field() }}

                        <button class="btn btn-success btn-sm"
                          data-toggle="tooltip" data-placement="top" title="Activar"
                          onclick="return confirm('Estas seguro de querer activar esta publicacion?')"
                        ><i class="fa fa-arrow-up"></i></button>

                        </form>
                        @endif
                        @endcan

                        </td>
                    </tr>
      
                    @endforeach
      
            </tbody>
            
          </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection


@push('styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- page script -->
<script>
    $(function () {
      $('#posts-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,

        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
            "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",           
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "search": "Buscar:",
              "paginate": {
              "first":      "Primer",
              "last":       "Ultimo",
              "next":       "Siguiente",
              "previous":   "Anterior"
        }
        }
      });
    });
  </script>

  @endpush