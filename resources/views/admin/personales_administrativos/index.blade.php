@extends('admin.layoutadmin')

@section('header')
<section class="content-header">
    <h1>
      Listado de Personal Administrativo
      <small>Todo el Personal Administrativo</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Personal Administrativo</li>
    </ol>
  </section>

  @endsection

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Listado de Personal Administrativo</h3>

      <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#personalModal">
        <i class="fa fa-plus"></i>Agregar Personal</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="personales_administrativos-table" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Email</th>
              <th>Acciones</th>
            </tr>
            </thead>

            <tbody>
                    @foreach($personales_administrativos as $personal_administrativo)
      
                    <tr>
                        <td>{{$personal_administrativo->id}}</td>
                        <td>{{$personal_administrativo->email}}</td>
                        <td>

                        <a href="{{ route('admin.personales_administrativos.edit', $personal_administrativo) }}"
                            class="btn btn-info btn-sm"
                            ><i class="fa fa-pencil"></i></a>

                        <form method="POST" 
                        action="{{ route('admin.personales_administrativos.destroy', $personal_administrativo) }}"
                        style="display: inline">
                        {{ csrf_field() }} {{ method_field('DELETE') }}

                        <button class="btn btn-danger btn-sm"
                          onclick="return confirm('Estas seguro de querer eliminar este personal?')"
                        ><i class="fa fa-times"></i></button>

                        </form>

                        </td>
                    </tr>
      
                    @endforeach
      
            </tbody>
            
          </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection


@push('styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- page script -->
<script>
    $(function () {
      $('#personales_administrativos-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "scrollX": true,

        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
            "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",           
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "search": "Buscar:",
              "paginate": {
              "first":      "Primer",
              "last":       "Ultimo",
              "next":       "Siguiente",
              "previous":   "Anterior"
        }
        }
      });
    });
  </script>

  @endpush