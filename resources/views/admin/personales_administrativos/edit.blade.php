@extends('admin.layoutadmin')

@section('header')
    <section class="content-header">
        <h1>
          PERSONALES ADMINISTRATIVOS
          <small>Agregar personal administrativo</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
          <li><a href="{{route('admin.personales_administrativos.index')}}"><i class="fa fa-list"></i> Personales Administrativos</a></li>
          <li class="active">Agregar</li>
        </ol>
    </section>
@stop

@section('content')

<div class="row">

    <form method="POST" action="{{route('admin.personales_administrativos.update', $personal_administrativo)}}">
        {{csrf_field()}} {{ method_field('PUT') }}
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('titulo') ? 'has-error': '' }}">
                            <label>Correo personal_administrativo</label>
                                    <input class="form-control" placeholder="Ingresa Correo" name="email"
                                    value="{{old('email', $personal_administrativo->email)}}">
                                    
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    
                        </div>

                    </div>

                </div>                
            </div>

        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">
        
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Guardar personal administrativo</button>
                    </div>

                </div>

            </div>
        </div>

    </form>

</div>               


@stop
