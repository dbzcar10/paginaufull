<!-- Modal -->
<div class="modal fade" id="personalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form method="POST" action="{{route('admin.personales_administrativos.store','#createPersonal') }}">
        {{csrf_field()}}

  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Correo de tu nuevo Personal</h4>
        </div>
        <div class="modal-body">
          
            <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                <input class="form-control" id="personal_administrativo-email" name="email"
                value="{{old('email')}}"
                placeholder="Ingresa email de tu personal_administrativo" autofocus required type="email">
                
                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary">Crear Personal</button>
        </div>
      </div>
    </div>
    </form>
  </div>

  @push('scripts')
   <script>

      if(window.location.hash === '#createPersonal')
       {
         $('#personalModal').modal('show');
       }
    
       $('#personalModal').on('hide.bs.modal', function(){
          window.location.hash = '#';
    
       });
    
       $('#personalModal').on('shown.bs.modal', function(){
         $('#personal_administrativo-email').focus();
          window.location.hash = '#createPersonal';
    
       }); 
    </script>
  @endpush