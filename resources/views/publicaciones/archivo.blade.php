@if(str_contains($publicacion->archivos->first()->url, '.jpg') || str_contains($publicacion->archivos->first()->url, '.png') || 
    str_contains($publicacion->archivos->first()->url, '.gif') || str_contains($publicacion->archivos->first()->url, '.svg') ||
    str_contains($publicacion->archivos->first()->url, '.bmp') || str_contains($publicacion->archivos->first()->url, '.jpeg')||
    str_contains($publicacion->archivos->first()->url, 'tiff'))
    <img src=" {{asset( $publicacion->archivos->first()->url) }}" 
    class="img-responsive"
    alt="Foto: {{ $publicacion->titulo }}"
    >
@endif
