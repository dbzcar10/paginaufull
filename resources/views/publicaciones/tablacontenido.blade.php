<div class="container">
    <table class="table table-bordered table-striped table-sm table-responsive-sm">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Titulo</th>
                <th>Accion</th>
            </tr>
        </thead>

        <tbody>
            @foreach($publicacion->archivos as $archivo)

            @if(str_contains($archivo->url, '.pdf'))      
                <tr>
                    <td><img src="{{asset('adminlte/img/pdf.svg')}}" width="25rem"></td>
                    <td><a href="{{ route('archivos.show', $archivo) }}" target="_blank" style="font-size:1rem"> {{$archivo->nombre}} </a></td>
                    <td><a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary">Descargar</a></td>
                </tr>

            @elseif (str_contains($archivo->url,'xls') || str_contains($archivo->url,'xlsx') )
                <tr>
                    <td><img src="{{asset('adminlte/img/excel.svg')}}" width="25rem"></td>
                    <td><a href="{{ route('archivos.show', $archivo) }}" target="_blank" style="font-size:1rem">{{$archivo->nombre}}</a></td>
                    <td><a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary">Descargar</a></td>
                </tr>

            @elseif (str_contains($archivo->url,'.doc') || str_contains($archivo->url,'.docx') )
                <tr>
                    <td><img src="{{asset('adminlte/img/word.svg')}}" width="25rem"></td>
                    <td><a href="{{ route('archivos.show', $archivo) }}" target="_blank" style="font-size:1rem">{{$archivo->nombre}}</a></td>
                    <td><a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary">Descargar</a></td>
                    
                </tr>

            @elseif(str_contains($archivo->url, '.zip') || str_contains($archivo->url, '.rar') )
                <tr>
                    <td><img src="{{asset('adminlte/img/rar.svg')}}" width="25rem"></td>
                    <td><a href="{{ route('archivos.show', $archivo) }}" target="_blank" style="font-size:1rem">{{$archivo->nombre}}</a></td>
                    <td><a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary">Descargar</a></td>
                </tr>

            @elseif(str_contains($archivo->url, '.jpg') || str_contains($archivo->url, '.png') || 
                    str_contains($archivo->url, '.gif') || str_contains($archivo->url, '.svg') ||
                    str_contains($archivo->url, '.bmp') || str_contains($archivo->url, '.jpeg')||
                    str_contains($archivo->url, 'tiff'))
            @else
                <tr>
                    <td><img src="{{asset('adminlte/img/archivo.svg')}}" width="25rem"></td>
                    <td><a href="{{ route('archivos.show', $archivo) }}" target="_blank" style="font-size:1rem">{{$archivo->nombre}}</a></td>
                    <td><a href="{{ route('archivos.descarga', $archivo) }}" class="btn-primary">Descargar</a></td>    
                </tr>
            @endif

            @endforeach

        </tbody>      
    </table>
                                    
</div>