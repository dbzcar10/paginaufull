<header class="container-flex space-between">
    <div class="date">
        <span class="c-gris">{{ optional($publicacion->publicado_el)->format('M d') }} / {{ $publicacion->propietario->name }}
        </span>
    </div>

    @if($publicacion->categoria)
        <div class="post-category">
            {{--<span class="category text-capitalize"> <a href="{{ route('categorias.show', $publicacion->categoria) }}" style="color:#ffffff" > {{ $publicacion->categoria->nombre}}</a> </span>--}}
            <span class="category text-capitalize"> <a href="#" style="color:#ffffff" > {{ $publicacion->categoria->nombre}}</a> </span>
        </div>
    @endif    
        
</header>