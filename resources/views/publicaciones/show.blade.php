@extends('layout')


@section('meta-title', $publicacion->titulo)
@section('meta-description', $publicacion->excerpt)

@section('content')

<article class="post container">
  @include( $publicacion->viewType())
        
  <div class="content-post">

    @include('publicaciones.header')

      <h1>{{ $publicacion->titulo }}</h1>
        <div class="divider"></div>

          <div class="image-w-text">

            {!! $publicacion->cuerpo !!}

              @if ($publicacion->archivos->count())

                @include('publicaciones.tablacontenido')
            
              @endif

            

          </div>
    
    <footer class="container-flex space-between">

      @include('partials.social-links', ['description' => $publicacion->titulo])

    </footer>
      <div class="comments">

        <div class="divider"></div>
          <div id="disqus_thread"></div>

      </div><!-- .comments -->
  </div>
  
</article>

      @include('partials.disqus-script')
@endsection

@push('styles')
      <link rel="stylesheet" type="text/css" href="{{asset('css/twitter-bootstrap.css')}}">
@endpush

@push('scripts')
      <script id="dsq-count-scr" src="//umg-2.disqus.com/count.js" async></script>  
   <script>src="{{asset('js/twitter-bootstrap.js')}}"</script>
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


@endpush 