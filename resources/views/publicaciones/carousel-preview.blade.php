{{--<div class="gallery-photos" data-masonry='{ "itemSelector": ".container"}'>

	@foreach($publicacion->archivos->take(4) as $archivo)
		<figure class="container">

			<div class="col-md-auto">
				@if($loop->iteration ===4)
					<div class="overlay" > {{ $publicacion->archivos->count() }} Fotos></div>
				@endif
					<img src=" {{ url( $archivo->url) }}">
			</div>
			
		</figure>
	@endforeach

</div>--}}
	
	<style> 
        .container { 
            background: # f9f9f9; 
        } 
		
		.flex-column { 
		max-width: 260px; 
		} 
		
        img { 
            margin: 5px; 
        } 
	</style>
    
	<div class = "container">
		<div class = "d-flex flex-row flex-wrap justify-content-center">
			
			@foreach($publicacion->archivos->take(6) as $archivo)
				<div class = "d-flex flex-column">

					@if(str_contains($archivo->url, '.jpg') || str_contains($archivo->url, '.png') || 
                		str_contains($archivo->url, '.gif') || str_contains($archivo->url, '.svg') ||
                		str_contains($archivo->url, '.bmp') || str_contains($archivo->url, '.jpeg')||
                		str_contains($archivo->url, 'tiff'))
						<img src="{{ url( $archivo->url) }}" class="img-fluid">
					@endif
					
					
					@if($loop->iteration ===6)
						<div class="overlay"> {{ $publicacion->archivos->count()}} Archivos></div>
					@endif

				</div>
										
			@endforeach
			 
			</div>
		</div>
	</div>
   