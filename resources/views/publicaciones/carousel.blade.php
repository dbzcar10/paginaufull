<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach($publicacion->archivos as $archivo)
        <li data-target="#carousel-example-generic" 
            data-slide-to="{{ $loop->index }}" 
            class="{{ $loop->first ? 'active' : '' }}">
        </li>
        @endforeach
    </ol>



    <div class="carousel-inner">
        @foreach($publicacion->archivos as $archivo)
            
            @if(str_contains($archivo->url, '.jpg') || str_contains($archivo->url, '.png') || 
                str_contains($archivo->url, '.gif') || str_contains($archivo->url, '.svg') ||
                str_contains($archivo->url, '.bmp') || str_contains($archivo->url, '.jpeg')||
                str_contains($archivo->url, 'tiff'))

            <div class="carousel-item {{ $loop->first ? 'active' : ''}}">
                <img src="{{ url($archivo->url) }}">
            </div>
            
            @else
                <div class="carousel-item {{ $loop->first ? 'active' : ''}}">
                <img src="{{asset('adminlte/img/archivo.svg')}}" alt="archivo"  height="100rem">
                </div>
            
            @endif
        
        @endforeach
    </div>

     
    <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>

    <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>

  </div>






