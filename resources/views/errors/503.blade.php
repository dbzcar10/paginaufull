<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UMG mantenimiento</title>

    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <script
			  src="https://code.jquery.com/jquery-3.3.1.js"
			  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
			  crossorigin="anonymous"></script>
</head>
<body style="background-color:rgba(77,142,234,1)">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1 style="color:#f5680a">Disculpe las molestias</h1>
            </div>
            <div class="col-6">
                <img src="{{asset('img/umg.png')}}" width="140rem">
            </div>
            
        </div>  
        <div class="col-sm-12">
            <img src="{{asset('img/construccion.png')}}" width="320rem" class="img-fluid">
        </div>      
        
        <br>
        <h1>Pagina en mantenimiento</h1>
    </div>
   

    <script src="{{asset('bootstrap/js/bootstrap.bundle.js')}}"></script>
	<script src="{{asset('bootstrap/js/bootstrap.js')}}"></script>
</body>
</html>