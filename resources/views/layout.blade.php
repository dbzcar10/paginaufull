<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>UMG</title>

	<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{asset('css/normalize.css')}}">
	<link rel="stylesheet" href="{{asset('css/framework.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/responsive.css')}}">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

  <script
			  src="https://code.jquery.com/jquery-3.3.1.js"
			  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
			  crossorigin="anonymous"></script>

     <script
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>>
    </script>
	
	<style>
	.dropdown-item:hover {
	background-color: #CC3433;
	color: white;
	}
	</style>

	@stack('chat_css')

	@stack('styles')
	@stack('scripts')

</head>

@include('partials.nav')

<body style="margin-top: 7rem">

	
<!--CONTENIDO DINAMICO-->
@yield('content')

<section class="footer">
		<footer>
			<div style="background-color:#334878;" class="container">
				<!--<figure class="logo"><img src="img/logo.png" alt=""></figure> -->

				<p><FONT FACE="verdana"> <h3>Contacto</h3>
					<br>
					Guastatoya, El Progreso <br>
					2ª calle 3-21 zona 5, Barrio El Porvenir
					<br>
					Tel. (502) 7945 2482, 7945 1925</FONT></p>
				<div class="divider-2" style="width: 80%;"></div>
				
				<ul class="social-media-footer list-unstyled">
					<li><a href="#" class="fb"></a></li>
					<li><a href="#" class="tw"></a></li>
					<li><a href="#" class="in"></a></li>
					<li><a href="#" class="pn"></a></li>
				</ul>
				<p><FONT FACE="verdana">© 2018 - Todos los Derechos Reservados. Diseñado y Elaborado Por <span class="c-white">LaaG y CardX</span></FONT></p>
			</div>
		</footer>
	</section>
			  
	<script src="{{asset('bootstrap/js/bootstrap.bundle.js')}}"></script>
	<script src="{{asset('bootstrap/js/bootstrap.js')}}"></script>

	@stack('scripts')
	@stack('chat_js')
</body>
</html>