@extends('layout')

@section('content')
<section class="pages container">
		<div class="page page-archive">
			<h1 class="text-capitalize">Archivo</h1>
			<div class="container-flex space-between">
				<div class="authors-categories">
					<h3 class="text-capitalize">Ultimos en publicar</h3>
					<ul class="list-unstyled">
						@foreach ($autores as $autor)
                            <li>{{ $autor->name }}</li>
                            
                        @endforeach 
					</ul>
					<h3 class="text-capitalize">Categorias</h3>
					<ul class="list-unstyled">

                    @foreach ($categorias as $categoria)
                        <a href=""><li class="text-capitalize">{{ $categoria->nombre }}</li></a>
                    @endforeach
						
					</ul>
				</div>
				<div class="latest-posts">
					<h3 class="text-capitalize">Ultimas publicaciones</h3>

                        @foreach ($publicaciones as $publicacion)
                            <a href="{{ route('publicaciones.show', $publicacion) }}"><p>{{$publicacion->titulo}}</p></a>
                        @endforeach
					
					<h3 class="text-capitalize">Publicaciones por fecha</h3>
					<ul class="list-unstyled">
                    <h5>PRINCIPAL:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 1)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.home', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach
                        <br>

                    <h5>AUDITORIA:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 2)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.ciencias_economicas', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach
                        <br>
                    
                    <h5>TRABAJO SOCIAL:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 3)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.trabajo_social', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach
                        <br>

                    <h5>INGENIERIA EN SISTEMAS:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 4)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.ingenieria', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach
                        <br>

                    <h5>CRIMINOLOGIA Y POLITICA CRIMINAL:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 5)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.escuela_criminalista', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach
                        <br>

                    <h5>ADMINISTRACIÓN:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 6)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.administracion', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach
                        <br>

                    <h5>DERECHO:</h5>
						@foreach ($archivo as $dato)

                        @if($dato->facultad_id == 7)
                            <li class="text-capitalize"> 
                                <a href="{{ route('pages.derecho', ['month' => $dato->month, 'year' => $dato->year]) }}">
                                    {{$dato->monthname}} {{$dato->year}} ({{$dato->publicaciones}})
                                </a>
                            </li>
                        @endif          
                            
                        @endforeach


					</ul>
				</div>
			</div>
		</div>
	</section>

@endsection