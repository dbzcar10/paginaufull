<div class="container"><img src="{{asset('img/derecho.png')}}" width="1000px" height="200px" class="img-responsive"></div>

@extends('layout')
@section('content')
	<section class="posts container">
			@if (isset ($titulo))
			<h3>{{ $titulo }}</h3>
			@endif


		@forelse($publicaciones as $publicacion)

		<article class="post">

			@include( $publicacion->viewType('home'))

			<div class="content-post">
				
				@include('publicaciones.header')

			
				<h1>{{$publicacion->titulo}}</h1>
				<div class="divider"></div>
				<p>{{$publicacion->extracto}}</p>
				
				<footer class="container-flex space-between">
					<div class="read-more">
						<a href="{{ route('publicaciones.show', $publicacion) }}" class="text-uppercase c-green">Leer mas</a>	</div>
					</div>

				</footer>
			</div>
		</article>

		@empty
			<article class="post">

				<div class="content-post">
								
					<h1>No hay publicaciones</h1>
					
				</div>
			</article>

		@endforelse

	</section><!-- fin del div.posts.container -->

	<div class="container">
		{{ $publicaciones->appends(request()->all())->links('vendor/pagination/default') }}
	</div>
	
    
    @stop
