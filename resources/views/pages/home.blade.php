
@extends('layout')
<!--CAROUSEL INICIO-->

<div class="row justify-content-center">
    <div class="col-sm-0"></div>

<div id="carouselExampleControls" class="carousel slide"  data-ride="carousel" >
	<div class="carousel-inner">
	  <div class="carousel-item active">
		<img class="d-block w-100" src="{{asset('img/sede-frente.png')}}" alt="First slide">
	  </div>
	  <div class="carousel-item">
		<img class="d-block w-100" src="{{asset('img/sede-lateral.png')}}" alt="Second slide">
	  </div>
	  
	</div>
	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	  <span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	  <span class="carousel-control-next-icon" aria-hidden="true"></span>
	  <span class="sr-only">Next</span>
	</a>
  </div>
</div>

@section('content')
	<section class="posts container">
		
			@if (isset ($titulo))
			<h3>{{ $titulo }}</h3>
			@endif


		@forelse($publicaciones as $publicacion)

		<article class="post">

			@include( $publicacion->viewType('home'))

			<div class="content-post">
				
				@include('publicaciones.header')

			
				<h1>{{$publicacion->titulo}}</h1>
				<div class="divider"></div>
				<p>{{$publicacion->extracto}}</p>
				
				<footer class="container-flex space-between">
					<div class="read-more">
						<a href="{{ route('publicaciones.show', $publicacion) }}" class="text-uppercase c-green">Leer mas</a>	</div>
					</div>

				</footer>
			</div>
		</article>

		@empty
			<article class="post">

				<div class="content-post">
								
					<h1>No hay publicaciones</h1>
					
				</div>
			</article>

		@endforelse

	</section><!-- fin del div.posts.container -->

	<div class="container">
		{{ $publicaciones->appends(request()->all())->links('vendor/pagination/default') }}
	</div>
	
    
    @stop
