<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color:#334878;">
		<a class="navbar-brand" href="#"> <img src="{{asset('img/50anios.png')}}" width="215rem" alt="Logo" /> </a>
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		</button>
	  
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		  <ul class="navbar-nav ml-auto">

		
			<li class="nav-item active">
            <a class="nav-link" href="{{route('pages.home') }}"
            class="pure-menu-link c-gris-2 text-uppercase {{ setActiveRoute('pages.inicio') }}">
           <b><font color="white"> INICIO</font></b> <span class="sr-only">(current)</span></a>
				
			</li>

			
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="{{route('pages.facultades') }}"
				class="pure-menu-link c-gris-2 text-uppercase {{ setActiveRoute('pages.facultades') }}" 
				id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<b><font color="white">FACULTADES</font></b>
				</a>
			  <div class= "dropdown-menu" aria-labelledby="navbarDropdown">
				
				<a class="dropdown-item" href={{route('pages.ciencias_economicas')}}>AUDITORIA</a>
				<a class="dropdown-item" href={{route('pages.trabajo_social')}}>TRABAJO SOCIAL</a>
				<a class="dropdown-item" href={{route('pages.ingenieria')}}>INGENIERIA EN SISTEMAS</a>
				<a class="dropdown-item" href={{route('pages.escuela_criminalista')}}>CRIMINOLOGIA Y POLITICA CRIMINAL </a>
				<a class="dropdown-item" href={{route('pages.administracion')}}>ADMINISTRACIÓN </a>
				<a class="dropdown-item" href={{route('pages.derecho')}}>DERECHO </a>
			  </div>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="{{route('pages.archivo') }}"class="pure-menu-link c-gris-2 text-uppercase {{ setActiveRoute('pages.archivo') }}"><b><font color="white">ARCHIVO</font></b> <span class="sr-only">(current)</span></a>
			</li>

			{{--
			@if(!Auth::guest())
			<li class="nav-item">
				<a class="nav-link" href="{{route('chat') }}"class="pure-menu-link c-gris-2 text-uppercase {{ setActiveRoute('chat') }}"><b><font color="white">CHAT</font></b> <span class="sr-only">(current)</span></a>
			</li>
			@endif --}}

			@if(Auth::guest())
			<li class="nav-item">
			<a class="nav-link" href="{{route('login')}}"><b><font color="white">LOGIN</font></b><span class="sr-only">(current)</span></a>
			</li>
			@endif

			@if(!Auth::guest())
			<li class="nav-item">
					<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    			<b><font color="white">CERRAR SESION</font></b><span class="sr-only"></span>
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
				</form>
			</li>
			@endif
					
			
			</ul>
			
		<!--  <form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>

			</form> -->
			
		</div>
	  </nav>