<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group([
    'prefix'=>'admin',
    'namespace'=>'Admin',
    'middleware'=>['auth', 'umg', 'estado'] ],
function(){ 
    Route::get('/','AdminController@index')->name('dashboard');

    Route::get('publicaciones', 'PublicacionesController@index')->name('admin.publicaciones.index');
    Route::post('publicaciones', 'PublicacionesController@store')->name('admin.publicaciones.store');
    Route::get('publicaciones/{publicacion}', 'PublicacionesController@edit')->name('admin.publicaciones.edit');
    Route::put('publicaciones/{publicacion}', 'PublicacionesController@update')->name('admin.publicaciones.update');
    Route::get('publicaciones/create', 'PublicacionesController@create')->name('admin.publicaciones.create');
    Route::delete('publicaciones/{publicacion}', 'PublicacionesController@destroy')->name('admin.publicaciones.destroy');
    Route::post('publicaciones/{publicacion}/activar', 'PublicacionesController@activar')->name('admin.publicaciones.activar');

    Route::post('publicaciones/{publicacion}/archivos', 'ArchivosController@store')->name('admin.publicaciones.archivos.store');
    Route::delete('archivos/{archivo}', 'ArchivosController@destroy')->name('admin.archivos.destroy');
    

    Route::get('catedraticos', 'CatedraticosController@index')->name('admin.catedraticos.index');
    Route::post('catedraticos', 'CatedraticosController@store')->name('admin.catedraticos.store');
    Route::get('catedraticos/{catedratico}', 'CatedraticosController@edit')->name('admin.catedraticos.edit');
    Route::put('catedraticos/{catedratico}', 'CatedraticosController@update')->name('admin.catedraticos.update');
    Route::get('catedraticos/create', 'CatedraticosController@create')->name('admin.catedraticos.create');
    Route::delete('catedraticos/{catedratico}', 'CatedraticosController@destroy')->name('admin.catedraticos.destroy');

    Route::get('personales_administrativos', 'PersonalesAdministrativosController@index')->name('admin.personales_administrativos.index');
    Route::post('personales_administrativos', 'PersonalesAdministrativosController@store')->name('admin.personales_administrativos.store');
    Route::get('personales_administrativos/{personal_administrativo}', 'PersonalesAdministrativosController@edit')->name('admin.personales_administrativos.edit');
    Route::put('personales_administrativos/{personal_administrativo}', 'PersonalesAdministrativosController@update')->name('admin.personales_administrativos.update');
    Route::get('personales_administrativos/create', 'PersonalesAdministrativosController@create')->name('admin.personales_administrativos.create');
    Route::delete('personales_administrativos/{personal_administrativo}', 'PersonalesAdministrativosController@destroy')->name('admin.personales_administrativos.destroy');

    /*Route::get('facultades', 'FacultadesController@index')->name('admin.facultades.index');
    Route::post('facultades', 'FacultadesController@store')->name('admin.facultades.store');
    Route::get('facultades/create', 'FacultadesController@create')->name('admin.facultades.create');
    Route::delete('facultades/{facultad}', 'FacultadesController@destroy')->name('admin.facultades.destroy');
    Route::put('facultades/{facultad}', 'FacultadesController@update')->name('admin.facultades.update');
    Route::get('facultades/{facultad}', 'FacultadesController@edit')->name('admin.facultades.edit');*/

    Route::resource('facultades','FacultadesController',['except'=>'show', 'as'=>'admin', 'singular'=>'facultad'])->parameters([
        'facultades' =>'facultad'
    ]);

    Route::resource('users', 'UsersController',['as' => 'admin']);

    Route::middleware('role:Admin')
    ->put('users/{user}/roles', 'UsersRolesController@update')
    ->name('admin.users.roles.update');

    Route::middleware('role:Admin')
    ->put('users/{user}/permissions', 'UsersPermissionsController@update')
    ->name('admin.users.permissions.update');

    Route::get('modo_mantenimiento', 'AdminController@mantenimiento')->name('admin.mantenimiento');

});

Route::get('descarga/{archivo}', 'Admin\ArchivosController@descarga')->name('archivos.descarga');
Route::get('ver/{archivo}', 'Admin\ArchivosController@show')->name('archivos.show');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//Registration Routes...
///Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/redirect', 'Auth\SocialAuthGoogleController@redirect');
Route::get('/callback', 'Auth\SocialAuthGoogleController@callback');

//RUTAS FRONTEND

Route::get('/', 'PagesController@home')->name('pages.home');    

Route::get('facultades', 'PagesController@facultades')->name('pages.facultades');   
Route::get('archivo', 'PagesController@archivo')->name('pages.archivo'); 



Route::get('blog/{publicacion}', 'PublicacionesController@show')->name('publicaciones.show');
Route::get('categorias/{categoria}', 'CategoriasController@show')->name('categorias.show');


Route::get('ingenieria', 'PagesController@ingenieria')->name('pages.ingenieria');
Route::get('ciencias_economicas', 'PagesController@ciencias_economicas')->name('pages.ciencias_economicas');
Route::get('trabajo_social', 'PagesController@trabajo_social')->name('pages.trabajo_social');
Route::get('escuela_criminalista', 'PagesController@escuela_criminalista')->name('pages.escuela_criminalista');
Route::get('ciencias_de_la_administracion', 'PagesController@ciencias_de_la_administracion')->name('pages.administracion');
Route::get('ciencias_juridicas_sociales', 'PagesController@ciencias_juridicas_sociales')->name('pages.derecho');

//RUTAS PARA EL CHAT
/*
Route::get('/conversaciones','PagesController@conversaciones')->middleware('auth')->name('chat');
Route::get('user/register',['uses'=>'UserController@register','as'=>'user.register']);
Route::resource('conversation','ConversationController');
Route::resource('message','MessageController');*/