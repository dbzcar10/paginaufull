<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Archivo extends Model
{
    protected $table = 'archivos';
    
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($archivo){
            $archivoPath = str_replace('storage', 'public', $archivo->url);//reemplaza palabra storage por public

            Storage::delete($archivoPath);
        });
    }

    public function scopeImagen($query)
    {
        $query->where('url', 'like', '.jpg')
              ->count();

                return $query;
    }
}
