<?php
namespace App\Services;
use App\SocialGoogleAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Catedratico;
use App\PersonalAdministrativo;

class SocialGoogleAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {


        $account = SocialGoogleAccount::whereProvider('google')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        }else {
            $account = new SocialGoogleAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'google'
            ]);
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'estado' => 1,
                    'facultad_id' => 1,
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1,10000)),
                ]);
            }
            $account->user()->associate($user);
            $account->save();

            $existCatedratico = Catedratico::where('email',$user->email)->first();
            $existPersonal = PersonalAdministrativo::where('email',$user->email)->first();

            if ($existCatedratico){
                $user->assignRole('Catedratico');
            }

            elseif($existPersonal){
                $user->assignRole('Admin');
            }

            else{
                $user->assignRole('Estudiante');
            }

            return $user;
        }
    }
}
