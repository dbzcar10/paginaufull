<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catedratico extends Model
{
    protected $table = 'catedraticos';
    protected $fillable = [
        'email',
    ];

}
