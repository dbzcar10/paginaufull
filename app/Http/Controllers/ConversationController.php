<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Conversation;
use LRedis;
use App\Http\Requests;
use Auth;
use App\User;


class ConversationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request){
        $c1 = Conversation::where('user1',Auth::user()->id)->where('user2',$request->user_id)->count();
        $c2 = Conversation::where('user2',Auth::user()->id)->where('user1',$request->user_id)->count();
        if($c1 != 0 || $c2 != 0)
            return redirect()->back();

        $c = new Conversation();
        $c->user1 = Auth::user()->id;
        $c->user2 = $request->user_id;
        $c->save();

        return redirect()->back();
    }
    public function show($id){
        $conversationp = Conversation::findOrFail($id);

        $users = User::where('id','!=',Auth::user()->id)->get();
        $conversations = Auth::user()->conversations();

        if($conversationp->user1 == Auth::user()->id || $conversationp->user2 == Auth::user()->id)

            return view('conversation.chat',compact('conversationp', 'conversations', 'users'));

        return redirect()->back();
    }
}
