<?php

namespace App\Http\Controllers\Auth;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Catedratico;
use App\PersonalAdministrativo;
use Socialite;
use Auth;
use Exception;
use App\Services\SocialGoogleAccountService;

class SocialAuthGoogleController extends Controller
{
    /**
   * Create a redirect method to google api.
   *
   * @return void
   */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @return callback URL from google
     */
    public function callback(SocialGoogleAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('google')->user());
        auth()->login($user);
        return redirect()->to('/admin');
    }

    /*public function callback()
    {
        //dd("Hole");
        try {


            $googleUser = Socialite::driver('google')->user();
            $existUser = User::where('email',$googleUser->email)->first();
            $catedratico = Catedratico::all();
            $existCatedratico = Catedratico::where('email',$googleUser->email)->first();
            $existPersonal = PersonalAdministrativo::where('email',$googleUser->email)->first();

            if(str_contains($googleUser->email, '@miumg.edu.gt')){

                if($existUser) {
                    Auth::loginUsingId($existUser->id);
                }

                else {
                    $user = new User;
                    $user->name = $googleUser->name;
                    $user->email = $googleUser->email;
                    $user->google_id = $googleUser->id;
                    $user->url = $googleUser->user['image']{'url'};
                    $user->password = md5(rand(1,10000));
                    $user->facultad_id = 1;
                    $user->estado = 1;
                    $user->save();
                    Auth::loginUsingId($user->id);

                    if ($existCatedratico){
                        $user->assignRole('Catedratico');
                    }

                    elseif($existPersonal){
                        $user->assignRole('Admin');
                    }

                    else{
                        $user->assignRole('Estudiante');
                    }

                }

                return redirect()->to('/admin');

            }

            else{
                return redirect()
                    ->route('login')
                    ->with('flash','Este correo no pertenecio a la UMG');
            }


        }
        catch (Exception $e) {
            return 'error:'.$e;
        }
    }*/
}
