<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Publicacion;

class CategoriasController extends Controller
{
    public function show(Categoria $categoria)
    {
        //return $categoria;
        /*$titulo = "Publicaciones de la categoria: {$categoria->nombre}";

        if(!Auth::guest()){

            $query = $categoria->publicaciones()->publicado();


            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(4)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(4)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(4)->paginate(10);
            }


            return view('pages.home',compact ('titulo','publicaciones'));            

        }

        else
        {
            $query = $categoria->publicaciones()->publicado();
 
            $publicaciones = $query->postInvitado(4)->paginate(10);
            return view('pages.home',compact ('titulo','publicaciones'));                            
        }*/

        return view('pages.home',[
            'titulo' => "Publicaciones de la categoria: {$categoria->nombre}",
            'publicaciones' => $categoria->publicaciones()->paginate()
        ]);
    }
}
