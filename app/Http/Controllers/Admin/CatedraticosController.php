<?php

namespace App\Http\Controllers\Admin;

use App\Catedratico;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CatedraticosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Catedratico $catedraticos)
    {
        $this->authorize('view',$catedraticos);

        $catedraticos = Catedratico::all();

        return view('admin.catedraticos.index', compact('catedraticos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $catedratico = Catedratico::create($request->all());
        return redirect()->route('admin.catedraticos.edit', $catedratico);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Catedratico $catedratico)
    {
        return view('admin.catedraticos.edit', [
            'catedratico' => $catedratico
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Catedratico $catedratico)
    {
        $catedratico->update($request->all());
      
  
        return redirect()
        ->route('admin.catedraticos.edit', $catedratico)
        ->with('flash','El catedratico ha sido guardado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Catedratico $catedratico)
    {
        $catedratico->delete();

        return redirect()
        ->route('admin.catedraticos.index')
        ->with('flash','El catedratico ha sido eliminado');
    }
}
