<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use \App\User;

class UsersRolesController extends Controller
{
    public function update(Request $request, User $user)
    {
        $user->roles()->detach();

        if ($request->filled('roles')) 
        {
            $user->assignRole($request->roles);
        }


        return back()->withFlash('Los roles han sido actualizados');
    }

}
