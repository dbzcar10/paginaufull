<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Publicacion;
use App\Archivo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ArchivosController extends Controller
{
    public function store(Publicacion $publicacion)
    {
        //$this->validate(request(),[
        //    'imagen' => 'required | image | max: 2048' //jpg. png, bmp, gif,  o svg
        //]);

        $archivo = request()->file('archivo')->store('publicaciones','public');
        $archivoRequest = request()->file('archivo');
        $nombreArchivo = $archivoRequest->getClientOriginalName();

        $publicacion->archivos()->create([
            'url' => Storage::url($archivo),
            'nombre' => $nombreArchivo,
        ]);

        /*

        $archivo = request()->file('archivo');
        $fileName = $archivo->getClientOriginalName();
        dd($fileName);*/

    }

    public function destroy(Archivo $archivo)
    {
       $archivo->delete();

       //Storage::disk('public')->delete($imagen->url);

       return back()->with('flash', 'Archivo eliminado');

    }    

    public function descarga(Archivo $archivo)
    {
        $public_path = public_path();
        $path1 = str_replace("/laravel", "", $public_path);
    
        $path = $path1."/".$archivo->url;
        
       return response()->download($path);
    }

    public function show(Archivo $archivo)
    {        
        $public_path = public_path();
        $path1 = str_replace("/laravel", "", $public_path);
    
        $path = $path1."/".$archivo->url;

       //dd($path);
       return response()->file($path);

    }
}
