<?php

namespace App\Http\Controllers\Admin;

use App\Facultad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacultadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Facultad $facultades)
    {
        $this->authorize('view',$facultades);

        $facultades = Facultad::all();
        return view('admin.facultades.index', compact('facultades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Facultad);

        $facultades = Facultad::create($request->all());
        return redirect()->route('admin.facultades.edit', $facultades);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Facultad $facultad)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Facultad $facultad)
    {
        $this->authorize('update', $facultad);
        return view('admin.facultades.edit', [
            'facultad' => $facultad
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facultad $facultad)
    {
        $this->authorize('update', $facultad);
        
        $facultad->update($request->all());
      
        return redirect()
        ->route('admin.facultades.edit', $facultad)
        ->with('flash','La facultad ha sido guardada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facultad $facultad)
    {
        $this->authorize('delete', $facultad);
        $facultad->delete();

        return redirect()
        ->route('admin.facultades.index')
        ->with('flash','La facultad ha sido eliminada');
    }
}
