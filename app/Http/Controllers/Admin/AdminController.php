<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Publicacion;
use App\Archivo;
use Artisan;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function index()
    {
        $publicaciones = Publicacion::all();
        $users = User::all();
        $archivos = Archivo::all();
        //dd($publicaciones);
        return view('admin.dashboard', compact('publicaciones', 'users', 'archivos'));
    }

    public function mantenimiento()
    {
        $this->authorize('Mantenimiento');

        $path = storage_path()."/"."framework/down";
        $existe = file_exists($path);

        if($existe)
        {
            Artisan::call('up');
            return redirect()
            ->route('dashboard')
            ->with('flash','El modo mantenimiento esta desactivado');
        }
        
        else
        {
            Artisan::call('down');
            return redirect()
            ->route('dashboard')
            ->with('flash','El modo mantenimiento esta activado');
        }
       
    }
}
