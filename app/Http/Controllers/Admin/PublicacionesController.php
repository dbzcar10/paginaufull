<?php

namespace App\Http\Controllers\Admin;

use App\Publicacion;
use App\Categoria;
use App\Facultad;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePublicacionRequest;

class PublicacionesController extends Controller
{
    public function index()
    {
        $publicaciones = Publicacion::permitido()->get();

        //$publicaciones = auth()->user()->publicaciones;
        return view('admin.publicaciones.index', compact('publicaciones')); 

        //return $publicaciones;
    }

    public function create()
    {
        $categorias = Categoria::all();
        $facultades = Facultad::all();
        return view('admin.publicaciones.create', compact('categorias', 'facultades'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', new Publicacion);

        $this->validate($request, ['titulo' => 'required|min:3']);

        $publicacion = Publicacion::create( $request->all() );

        return redirect()->route('admin.publicaciones.edit', $publicacion);

    }

    public function edit(Publicacion $publicacion)
    {
        $this->authorize('update', $publicacion);

        //$user = User::all()->where('id',auth()->id());

        $user = User::find(auth()->id());

        $facultad=$user->facultad_id;


        if($user->hasRole('Admin') || $user->hasRole('Catedratico'))
        {
            return view('admin.publicaciones.edit', [
                'publicacion' => $publicacion,
                'categorias' => Categoria::all(),
                'facultades' => Facultad::all()
            ]);
        }

        else
        {
            return view('admin.publicaciones.edit', [
                'publicacion' => $publicacion,
                'categorias' => Categoria::all(),
                'facultades' => Facultad::facultadpermitida($facultad)->get()
            ]);
        }


       

    }

    public function update(Publicacion $publicacion, StorePublicacionRequest $request)
    {
        $this->authorize('update', $publicacion);

        $publicacion->update($request->all());

        return redirect()
        ->route('admin.publicaciones.edit', $publicacion)
        ->with('flash','La publicacion ha sido guardada');
    }

    public function destroy(Publicacion $publicacion)
    {
        $this->authorize('delete', $publicacion);

        /*$publicacion->delete();*/

        $publicacion->estado = 0;
        $publicacion->update();

        return redirect()
        ->route('admin.publicaciones.index')
        ->with('flash','La publicacion ha sido eliminada');
    }

    public function activar(Publicacion $publicacion)
    {
        
        $this->authorize('delete', $publicacion);

        /*$publicacion->delete();*/

        $publicacion->estado = 1;
        $publicacion->update();

        return redirect()
        ->route('admin.publicaciones.index')
        ->with('flash','La publicacion ha sido activada');
    }
}
