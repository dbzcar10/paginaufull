<?php

namespace App\Http\Controllers\Admin;

use App\PersonalAdministrativo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PersonalesAdministrativosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PersonalAdministrativo $personales_administrativos)
    {
        $this->authorize('view',$personales_administrativos);

        $personales_administrativos = PersonalAdministrativo::all();

        return view('admin.personales_administrativos.index', compact('personales_administrativos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $personal_administrativo = PersonalAdministrativo::create($request->all());
        return redirect()->route('admin.personales_administrativos.edit', $personal_administrativo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalAdministrativo $personal_administrativo)
    {
        return view('admin.personales_administrativos.edit', [
            'personal_administrativo' => $personal_administrativo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonalAdministrativo $personal_administrativo)
    {
        $personal_administrativo->update($request->all());
      
  
        return redirect()
        ->route('admin.personales_administrativos.edit', $personal_administrativo)
        ->with('flash','El personal administrativo ha sido guardado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalAdministrativo $personal_administrativo)
    {
        $personal_administrativo->delete();

        return redirect()
        ->route('admin.personales_administrativos.index')
        ->with('flash','El personal administrativo ha sido eliminado');
    }
}
