<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publicacion;
use Illuminate\Support\Facades\Auth;
use App\Conversation;
use App\User;
use App\Categoria;

class PagesController extends Controller
{
    public function home (){
        $query = Publicacion::publicado()->publicacionactiva();

        if(request('month')){
            $query->whereMonth('publicado_el', request('month'));
        }

        if(request('year')){
            $query->whereYear('publicado_el', request('year'));
        }

        $publicaciones = $query->postAdmin(1)->paginate();

        //$publicaciones = Publicacion::publicado()->postAdmin(1)->paginate(10);
        return view('pages.home',compact ('publicaciones'));
    }


    public function ingenieria (){
        
        if(!Auth::guest()){

            $query = Publicacion::publicado()->publicacionactiva();

            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }

            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(4)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(4)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(4)->paginate(10);
            }


            return view('pages.ingenieria',compact ('publicaciones'));            

        }

        else
        {
            $query = Publicacion::publicado()->publicacionactiva();
            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }
    
            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            $publicaciones = $query->postInvitado(4)->paginate(10);
            return view('pages.ingenieria',compact ('publicaciones'));                            
        }

    }

    public function ciencias_economicas (){
        
        if(!Auth::guest()){

            $query = Publicacion::publicado()->publicacionactiva();

            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }

            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(2)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(2)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(2)->paginate(10);
            }


            return view('pages.ciencias_economicas',compact ('publicaciones'));            

        }

        else
        {
            $query = Publicacion::publicado()->publicacionactiva();
            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }
    
            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }
            
            $publicaciones = $query->postInvitado(2)->paginate(10);
            return view('pages.ciencias_economicas',compact ('publicaciones'));                            
        }

        
    }
    
    public function trabajo_social (){
        
        if(!Auth::guest()){

            $query = Publicacion::publicado()->publicacionactiva();

            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }

            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(3)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(3)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(3)->paginate(10);
            }


            return view('pages.trabajo_social',compact ('publicaciones'));            

        }

        else
        {
            $query = Publicacion::publicado()->publicacionactiva();
            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }
    
            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }
            
            $publicaciones = $query->postInvitado(3)->paginate(10);
            return view('pages.trabajo_social',compact ('publicaciones'));                            
        }        
    }

    public function escuela_criminalista (){
        if(!Auth::guest()){

            $query = Publicacion::publicado()->publicacionactiva();

            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }

            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(5)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(5)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(5)->paginate(10);
            }


            return view('pages.escuela_criminalista',compact ('publicaciones'));            

        }

        else
        {
            $query = Publicacion::publicado()->publicacionactiva();
            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }
    
            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }
            
            $publicaciones = $query->postInvitado(5)->paginate(10);
            return view('pages.escuela_criminalista',compact ('publicaciones'));                            
        }
        
    }

    public function ciencias_de_la_administracion (){
        
        if(!Auth::guest()){

            $query = Publicacion::publicado()->publicacionactiva();

            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }

            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(6)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(6)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(6)->paginate(10);
            }


            return view('pages.ciencias_de_la_administracion',compact ('publicaciones'));            

        }

        else
        {
            $query = Publicacion::publicado()->publicacionactiva();
            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }
    
            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }
            
            $publicaciones = $query->postInvitado(6)->paginate(10);
            return view('pages.ciencias_de_la_administracion',compact ('publicaciones'));                            
        }
        
    }

    public function ciencias_juridicas_sociales (){
        
        if(!Auth::guest()){

            $query = Publicacion::publicado()->publicacionactiva();

            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }

            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                $publicaciones = $query->postAdmin(7)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                $publicaciones = $query->postEstudiante(7)->paginate(10);
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                $publicaciones = $query->postInvitado(7)->paginate(10);
            }


            return view('pages.ciencias_juridicas_sociales',compact ('publicaciones'));            

        }

        else
        {
            $query = Publicacion::publicado()->publicacionactiva();
            if(request('month')){
                $query->whereMonth('publicado_el', request('month'));
            }
    
            if(request('year')){
                $query->whereYear('publicado_el', request('year'));
            }
            
            $publicaciones = $query->postInvitado(7)->paginate(10);
            return view('pages.ciencias_juridicas_sociales',compact ('publicaciones'));                            
        }
        
    }

    public function conversaciones()
    {
        $users = User::where('id','!=',Auth::user()->id)->get();
        $conversations = Auth::user()->conversations();
        return view('conversation.conversation',compact('users','conversations'));
    }



    public function facultades()
    {
        return view('pages.facultades');
    }

    public function archivo()
    {
        \DB::statement("SET lc_time_names = 'es_ES'");
        $archivo = Publicacion::selectRaw('year(publicado_el) year')
            ->selectRaw('month(publicado_el) month')
            ->selectRaw('monthname(publicado_el) monthname')
            ->selectRaw('facultad_id')
            ->selectRaw('count(*) publicaciones')
            ->groupBy('year','month', 'monthname', 'facultad_id')
            ->orderBy('year')
            ->get();

        //return $archivo;

        //$archivo = Publicacion::byYearAndMonth()->get();

        return view('pages.archivo', [
            'autores' => User::latest()->take(5)->get(),
            'categorias' => Categoria::take(7)->get(),
            'publicaciones' => Publicacion::latest('publicado_el')->take(6)->get(),
            'archivo' => $archivo
        ]);
    }
}
