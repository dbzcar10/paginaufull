<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publicacion;
use Illuminate\Support\Facades\Auth;


class PublicacionesController extends Controller
{
    public function show(Publicacion $publicacion)
    {
        if(!Auth::guest()){

            $idFacultad =Auth::user()->facultad->id;

            if(auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Catedratico'))
            {
                if($publicacion->isPublished() && $publicacion->estado==1)
                {
                    return view('publicaciones.show', compact('publicacion'));
                }
        
                abort(404);
            }

            elseif(auth()->user()->hasRole('Estudiante'))
            {
                if(
                    $publicacion->isPublished() && $publicacion->segu_externa == 'Publica' && $publicacion->segu_interna == 'Publica' && $publicacion->estado==1 || 
                    $publicacion->isPublished() && $publicacion->segu_externa == 'Privada' && $publicacion->segu_interna == 'Publica' && $publicacion->estado==1 ||
                    $publicacion->isPublished() && $publicacion->segu_externa == 'Privada' && $publicacion->segu_interna == 'Privada' && $publicacion->id == $idFacultad && $publicacion->estado==1 ||
                    $publicacion->isPublished() && $publicacion->facultad_id == 1 && $publicacion->estado==1)
                {
                    return view('publicaciones.show', compact('publicacion'));
                }
        
                abort(404);
                
            }

            elseif(auth()->user()->hasRole('Publico'))
            {
                if($publicacion->isPublished() && $publicacion->segu_externa == 'Publica' && $publicacion->estado==1 || $publicacion->isPublished() && $publicacion->facultad_id == 1 && $publicacion->estado==1)
                {
                    return view('publicaciones.show', compact('publicacion'));
                }
        
                abort(404);
            }
        }

        else
        {
            if($publicacion->isPublished() && $publicacion->segu_externa == 'Publica' && $publicacion->estado==1 || $publicacion->isPublished() && $publicacion->facultad_id == 1 && $publicacion->estado==1)
            {
                return view('publicaciones.show', compact('publicacion'));
            }
    
            abort(404);                         
        }    

    }
}
