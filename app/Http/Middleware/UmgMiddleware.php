<?php

namespace App\Http\Middleware;

use Closure;

class UmgMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            if (auth()->check() && !auth()->user()->hasRole('Publico'))
            return $next($request);

            return redirect('/');
        }

        catch(FormException $ex){
            exit('FormException caught!');
            return redirect('/');
        }
        
    }
}
