<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    protected $table = 'facultades';

    protected $fillable = [
        'nombre',
    ];

    public function publicaciones()
    {
        return $this->hasMany(Publicacion::class);
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function scopeFacultadPermitida($query, $user)
    {
        return $query->where('id', $user);
    }
}
