<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facultad_id', 'estado',
    ];

    public function facultad()
    {
        return $this->belongsTo(Facultad::class); //usuario pertenece a facultad
    }
    
    public function publicaciones()
    {
        return $this->hasMany(Publicacion::class); //usuario tiene muchas publicaciones
    }

    public function conversations()
    {
        return \App\Conversation::where('user1',$this->id)->orWhere('user2',$this->id)->get();
    }
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function scopePermitido($query)
    {
        if( auth()->user()->can('view', $this) )
        {
            return $query;
        }

        return $query->where('id', auth()->id());
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
