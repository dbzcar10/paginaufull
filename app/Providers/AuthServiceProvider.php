<?php

namespace App\Providers;

use App\Publicacion;
use App\Policies\PublicacionPolicy;
use App\Facultad;
use App\Policies\FacultadPolicy;
use App\Catedratico;
use App\Policies\CatedraticoPolicy;
use App\PersonalAdministrativo;
use App\Policies\PersonalPolicy;

use App\User;
use App\Policies\UserPolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Publicacion::class => PublicacionPolicy::class,
        Facultad::class => FacultadPolicy::class,
        Catedratico::class => CatedraticoPolicy::class,
        User::class => UserPolicy::class,
        PersonalAdministrativo::class => PersonalPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
