<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'url';

    }

    public function publicaciones()
    {
        return $this->hasMany(Publicacion::class);
    }

    public function setNombreAttribute($nombre)
    {
        $this->attributes['nombre'] = $nombre;
        $this->attributes['url'] = str_slug($nombre);
    }
}
