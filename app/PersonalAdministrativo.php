<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalAdministrativo extends Model
{
    protected $table = 'personales_administrativos';
    protected $fillable = [
        'email',
    ];
}
