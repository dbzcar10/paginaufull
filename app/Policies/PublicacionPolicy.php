<?php

namespace App\Policies;

use App\User;
use App\Publicacion;
use Illuminate\Auth\Access\HandlesAuthorization;

class PublicacionPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ( $user->hasRole('Admin') )
        {
            return true;
        }
    }

    /**
     * Determine whether the user can view the publicacion.
     *
     * @param  \App\User  $user
     * @param  \App\Publicacion  $publicacion
     * @return mixed
     */
    public function view(User $user, Publicacion $publicacion)
    {
        return $user->id === $publicacion->user_id
            || $user->hasPermissionTo('Ver publicaciones');
    }

    /**
     * Determine whether the user can create publicacions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Crear publicaciones');
    }

    /**
     * Determine whether the user can update the publicacion.
     *
     * @param  \App\User  $user
     * @param  \App\Publicacion  $publicacion
     * @return mixed
     */
    public function update(User $user, Publicacion $publicacion)
    {
        return $user->id === $publicacion->user_id
            || $user->hasPermissionTo('Actualizar publicaciones');
    }

    /**
     * Determine whether the user can delete the publicacion.
     *
     * @param  \App\User  $user
     * @param  \App\Publicacion  $publicacion
     * @return mixed
     */
    public function delete(User $user, Publicacion $publicacion)
    {
        return $user->id === $publicacion->user_id
            || $user->hasPermissionTo('Borrar publicaciones');
    }
}
