<?php

namespace App\Policies;

use App\User;
use App\Catedratico;
use Illuminate\Auth\Access\HandlesAuthorization;

class CatedraticoPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ( $user->hasRole('Admin') )
        {
            return true;
        }
    }
    /**
     * Determine whether the user can view the catedratico.
     *
     * @param  \App\User  $user
     * @param  \App\Catedratico  $catedratico
     * @return mixed
     */
    public function view(User $user, Catedratico $catedratico)
    {
        //
    }

    /**
     * Determine whether the user can create catedraticos.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the catedratico.
     *
     * @param  \App\User  $user
     * @param  \App\Catedratico  $catedratico
     * @return mixed
     */
    public function update(User $user, Catedratico $catedratico)
    {
        //
    }

    /**
     * Determine whether the user can delete the catedratico.
     *
     * @param  \App\User  $user
     * @param  \App\Catedratico  $catedratico
     * @return mixed
     */
    public function delete(User $user, Catedratico $catedratico)
    {
        //
    }
}
