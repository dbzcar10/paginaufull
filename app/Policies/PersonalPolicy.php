<?php

namespace App\Policies;

use App\User;
use App\PersonalAdministrativo;
use Illuminate\Auth\Access\HandlesAuthorization;

class PersonalPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ( $user->hasRole('Admin') )
        {
            return true;
        }
    }

    /**
     * Determine whether the user can view the personal administrativo.
     *
     * @param  \App\User  $user
     * @param  \App\PersonalAdministrativo  $personalAdministrativo
     * @return mixed
     */
    public function view(User $user, PersonalAdministrativo $personalAdministrativo)
    {
        //
    }

    /**
     * Determine whether the user can create personal administrativos.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the personal administrativo.
     *
     * @param  \App\User  $user
     * @param  \App\PersonalAdministrativo  $personalAdministrativo
     * @return mixed
     */
    public function update(User $user, PersonalAdministrativo $personalAdministrativo)
    {
        //
    }

    /**
     * Determine whether the user can delete the personal administrativo.
     *
     * @param  \App\User  $user
     * @param  \App\PersonalAdministrativo  $personalAdministrativo
     * @return mixed
     */
    public function delete(User $user, PersonalAdministrativo $personalAdministrativo)
    {
        //
    }

    /**
     * Determine whether the user can restore the personal administrativo.
     *
     * @param  \App\User  $user
     * @param  \App\PersonalAdministrativo  $personalAdministrativo
     * @return mixed
     */
    public function restore(User $user, PersonalAdministrativo $personalAdministrativo)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the personal administrativo.
     *
     * @param  \App\User  $user
     * @param  \App\PersonalAdministrativo  $personalAdministrativo
     * @return mixed
     */
    public function forceDelete(User $user, PersonalAdministrativo $personalAdministrativo)
    {
        //
    }
}
