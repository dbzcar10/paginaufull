<?php

namespace App\Policies;

use App\User;
use App\Facultad;
use Illuminate\Auth\Access\HandlesAuthorization;

class FacultadPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ( $user->hasRole('Admin') )
        {
            return true;
        }
    }

    /**
     * Determine whether the user can view the facultad.
     *
     * @param  \App\User  $user
     * @param  \App\Facultad  $facultad
     * @return mixed
     */
    public function view(User $user, Facultad $facultad)
    {
        //return false;
    }

    /**
     * Determine whether the user can create facultads.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the facultad.
     *
     * @param  \App\User  $user
     * @param  \App\Facultad  $facultad
     * @return mixed
     */
    public function update(User $user, Facultad $facultad)
    {
        //
    }

    /**
     * Determine whether the user can delete the facultad.
     *
     * @param  \App\User  $user
     * @param  \App\Facultad  $facultad
     * @return mixed
     */
    public function delete(User $user, Facultad $facultad)
    {
        //
    }
}
