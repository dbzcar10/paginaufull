<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Publicacion extends Model
{
    protected $table = 'publicaciones';

    protected $fillable = [
        'titulo', 'cuerpo', 'extracto', 'publicado_el', 'categoria_id', 'user_id', 'facultad_id', 'segu_externa', 'segu_interna', 'estado'];

    protected $dates = ['publicado_el'];

    public function getRouteKeyName()
    {
        return 'url';
    }

    
    public function categoria() //$publicacion->categoria->nombre
    {
        return $this->belongsTo(Categoria::class);
    }

    public function facultad()
    {
        return $this->belongsTo(Facultad::class);
    }

    public function archivos()
    {
        return $this->hasMany(Archivo::class);// 1 publicacion puede tener varios archivos
    }

    public function propietario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopePublicado($query)
    {
        $query->whereNotNull('publicado_el')
                    ->where('publicado_el', '<=', Carbon::now())
                    ->latest('publicado_el');
    }


    public function scopePostAdmin($query,$facultad)
    {
        $query->select('publicaciones.*')
        ->where('publicaciones.facultad_id', '=', $facultad)
        ->get();

            return $query;
    }

    public function scopePostEstudiante($query, $facultad)
    {
        $query->select('publicaciones.*')
        ->where('facultad_id', '=', $facultad)
        ->where('publicaciones.segu_externa', '=', 'Publica')
        ->where('publicaciones.segu_interna', '=', 'Publica')
        ->orWhere('facultad_id', '=', $facultad)
        ->where('publicaciones.segu_externa', '=', 'Privada')
        ->where('publicaciones.segu_interna', '=', 'Publica')
        ->orWhere('facultad_id', '=', $facultad)
        ->where('publicaciones.segu_externa', '=', 'Privada')
        ->where('publicaciones.segu_interna', '=', 'Privada')
        ->where('publicaciones.facultad_id', '=', auth()->user()->facultad_id);
            return $query;
    }

    public function scopePostInvitado($query, $facultad)
    {
        $query->select('publicaciones.*')
        ->where('publicaciones.facultad_id', '=', $facultad)
        ->where('publicaciones.segu_externa', '=', 'Publica')       
        ->get();

            return $query;
    }

    public function scopePublicacionActiva($query)
    {
        $query->select('publicaciones.*')
        ->where('publicaciones.estado', '=', 1)      
        ->get();

            return $query;
    }

    public function scopePermitido($query)
    {
        if( auth()->user()->can('view', $this))
        {
            return $query;
        }

        else
        {
            return $query->where('user_id', auth()->id());
        }
    }

    /*public function scopeByYearAndMonth($query)
    {
        \DB::statement("SET lc_time_names = 'es_ES'");
        $query->selectRaw('year(publicado_el) year')
            ->selectRaw('month(publicado_el) month')
            ->selectRaw('monthname(publicado_el) monthname')
            ->selectRaw('count(*) publicaciones')
            ->groupBy('year','month', 'monthname')
            ->orderBy('year')
            ->get();

        return $query;
        
    }*/


    public function isPublished()
    {
        return ! is_null($this->publicado_el) && $this->publicado_el < today();
    }

    public function setPublicadoElAttribute($publicado_el)
    {
        $this->attributes['publicado_el'] = $publicado_el ? Carbon::parse($publicado_el) :null;
    }

    public function setCategoriaIdAttribute($categoria)
    {
        $this->attributes['categoria_id'] = Categoria::find($categoria)
                                ? $categoria
                                : Categoria::create(['nombre' => $categoria])->id;
    }

    public static function create(array $attributes = [])
    {
        $attributes['user_id'] = auth()->id();
        $attributes['estado'] = 1;
        
        $publicacion = static::query()->create($attributes);

        $publicacion->generarUrl();

        return $publicacion;
    }

    public function viewType($home = '')
    {
        if ($this->archivos->count()=== 1):
            return 'publicaciones.archivo';
        elseif($this->archivos->count() > 1):

            return $home === 'home' ? 'publicaciones.carousel-preview' : 'publicaciones.carousel';
    
        elseif($this->iframe):
            return 'publicaciones.iframe';
        else:
            return 'publicaciones.text';
        endif;
    }

    public function generarUrl()
    {
        $url = str_slug( $this->titulo);

        if($this::whereUrl($url)->exists())
        {

            $url = "{$url}-{$this->id}";

            if($this::where('url', $url)->exists())
            {
                $url = "{$url}_{$this->id}";
            }

        }
        $this->url = $url;

        $this->save();  
    }
}
