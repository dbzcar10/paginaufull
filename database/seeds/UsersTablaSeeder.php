<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UsersTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();
        Role::truncate();
        User::truncate();

        $adminRole = Role::create(['name' => 'Admin']);
        $estudianteRole = Role::create(['name' => 'Estudiante']);
        $publicoRole = Role::create(['name' => 'Publico']);
        $catedraticoRole = Role::create(['name' => 'Catedratico']);

        $verPublicacionesPermission = Permission::create(['name' => 'Ver publicaciones']);
        $crearPublicacionesPermission = Permission::create(['name' => 'Crear publicaciones']);
        $actualizarPublicacionesPermission = Permission::create(['name' => 'Actualizar publicaciones']);
        $borrarPublicacionesPermission = Permission::create(['name' => 'Borrar publicaciones']);

        $viewUsersPermission = Permission::create(['name' => 'Ver usuarios']);
        $createUsersPermission = Permission::create(['name' => 'Crear usuarios']);
        $updateUsersPermission = Permission::create(['name' => 'Actualizar usuarios']);
        $deleteUsersPermission = Permission::create(['name' => 'Borrar usuarios']);

        $mantenimiento =  Permission::create(['name' => 'Mantenimiento']);

        $crearPublicaExternaPermission = Permission::create(['name' => 'Crear publica externa']);

        $admin = new User;
        $admin->name = 'Benjamin';
        $admin->email = 'cardx00@gmail.com';
        $admin->password = bcrypt('patito');
        $admin->facultad_id =1;
        $admin->estado = 1;
        $admin->save();

        $admin->assignRole($adminRole);
        $adminRole->givePermissionTo($crearPublicaExternaPermission);
        $adminRole->givePermissionTo($mantenimiento);


        $estudiante = new User;
        $estudiante->name = 'Carlos';
        $estudiante->email = 'cardx0@hotmail.com';
        $estudiante->password = bcrypt('patito');
        $estudiante->facultad_id=2;
        $estudiante->estado = 1;
        $estudiante->save();
        
        $estudiante->assignRole($estudianteRole);
        $estudianteRole->givePermissionTo($verPublicacionesPermission);
        //$estudianteRole->givePermissionTo($crearPublicacionesPermission);

        $estudiante2 = new User;
        $estudiante2->name = 'Estudiante 2';
        $estudiante2->email = 'estudiante@gmail.com';
        $estudiante2->password = bcrypt('patito');
        $estudiante2->facultad_id=3;
        $estudiante2->estado=1;
        $estudiante2->save();
        
        $estudiante2->assignRole($estudianteRole);


        $catedratico = new User;
        $catedratico->name = 'Catedratico';
        $catedratico->email = 'catedratico@gmail.com';
        $catedratico->password = bcrypt('patito');
        $catedratico->facultad_id=1;
        $catedratico->estado=1;
        $catedratico->save();
        
        $catedratico->assignRole($catedraticoRole);
        $catedraticoRole->givePermissionTo($verPublicacionesPermission);
        $catedraticoRole->givePermissionTo($crearPublicacionesPermission);
        $catedraticoRole->givePermissionTo($crearPublicaExternaPermission);


        $publico = new User;
        $publico->name = 'publico';
        $publico->email = 'publico@gmail.com';
        $publico->password = bcrypt('patito');
        $publico->facultad_id=1;
        $publico->estado=1;
        $publico->save();
        
        $publico->assignRole($publicoRole);
    }
}
