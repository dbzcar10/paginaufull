<?php

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(CategoriasTablaSeeder::class);
        $this->call(UsersTablaSeeder::class);
        $this->call(PublicacionesTablaSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');                     
    }
}
