<?php

use App\Publicacion;
use App\Categoria;
use Carbon\Carbon;
use App\Facultad;
use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Seeder;

class PublicacionesTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk('public')->deleteDirectory('publicaciones');

        Publicacion::truncate();
        Categoria::truncate();
        Facultad::truncate();

        $categoria = new Categoria;
        $categoria->nombre= "Categoria 1";
        $categoria->save();

        $categoria = new Categoria;
        $categoria->nombre= "Categoria 2";
        $categoria->save();

        $facultad = new Facultad;
        $facultad->nombre= "Sin asignar";
        $facultad->save();

        $facultad = new Facultad;
        $facultad->nombre= "CIENCIAS ECONOMICAS";
        $facultad->save();

        $facultad = new Facultad;
        $facultad->nombre= "ESCUELA DE TRABAJO SOCIAL";
        $facultad->save();

        $facultad = new Facultad;
        $facultad->nombre= "INGENIERIA EN SISTEMAS";
        $facultad->save();
		
		$facultad = new Facultad;
        $facultad->nombre= "ESCUELA DE CRIMINALISTICA";
        $facultad->save();
		
		$facultad = new Facultad;
        $facultad->nombre= "CIENCIAS DE LA ADMINISTRACION";
        $facultad->save();
		
		$facultad = new Facultad;
        $facultad->nombre= "CIENCIAS JURIDICAS Y SOCIALES";
        $facultad->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Primera Publicacion";
        $publicacion->url = str_slug("Mi Primera Publicacion");
        $publicacion->extracto= "Extracto de mi Primera Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Primera Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(4);
        $publicacion->categoria_id=1;
        $publicacion->facultad_id=1;
        $publicacion->user_id=1;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Publica";
        $publicacion->segu_interna = "Publica";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Segunda Publicacion";
        $publicacion->url = str_slug("Mi Segunda Publicacion");
        $publicacion->extracto= "Extracto de mi Segunda Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Segunda Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(3);
        $publicacion->categoria_id=1;
        $publicacion->facultad_id=1;
        $publicacion->user_id=1;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Publica";
        $publicacion->save();


        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Tercera Publicacion";
        $publicacion->url = str_slug("Mi Tercera Publicacion");
        $publicacion->extracto= "Extracto de mi Tercera Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Tercera Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(2);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();


        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Cuarta Publicacion";
        $publicacion->url = str_slug("Mi Cuarta Publicacion");
        $publicacion->extracto= "Extracto de mi Cuarta Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Cuarta Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Quinta Publicacion";
        $publicacion->url = str_slug("Mi Quinta Publicacion");
        $publicacion->extracto= "Extracto de mi Quinta Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Quinta Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Sexta Publicacion";
        $publicacion->url = str_slug("Mi Sexta Publicacion");
        $publicacion->extracto= "Extracto de mi Sexta Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Sexta Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Septima Publicacion";
        $publicacion->url = str_slug("Mi Septima Publicacion");
        $publicacion->extracto= "Extracto de mi Septima Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Septima Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Octava Publicacion";
        $publicacion->url = str_slug("Mi Octava Publicacion");
        $publicacion->extracto= "Extracto de mi Octava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Octava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Novena Publicacion";
        $publicacion->url = str_slug("Mi Novena Publicacion");
        $publicacion->extracto= "Extracto de mi Novena Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Novena Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Decima Publicacion";
        $publicacion->url = str_slug("Mi Decima Publicacion");
        $publicacion->extracto= "Extracto de mi Decima Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Decima Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Onceava Publicacion";
        $publicacion->url = str_slug("Mi Onceava Publicacion");
        $publicacion->extracto= "Extracto de mi Onceava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Onceava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Doceava Publicacion";
        $publicacion->url = str_slug("Mi Doceava Publicacion");
        $publicacion->extracto= "Extracto de mi Doceava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Doceava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Treceava Publicacion";
        $publicacion->url = str_slug("Mi Treceava Publicacion");
        $publicacion->extracto= "Extracto de mi Treceava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Treceava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Catorceava Publicacion";
        $publicacion->url = str_slug("Mi Catorceava Publicacion");
        $publicacion->extracto= "Extracto de mi Catorceava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Catorceava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Quinceava Publicacion";
        $publicacion->url = str_slug("Mi Quinceava Publicacion");
        $publicacion->extracto= "Extracto de mi Quinceava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Quinceava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();

        $publicacion = new Publicacion;
        $publicacion->titulo = "Mi Dieciseisava Publicacion";
        $publicacion->url = str_slug("Mi Dieciseisava Publicacion");
        $publicacion->extracto= "Extracto de mi Dieciseisava Publicacion";
        $publicacion->cuerpo="<p>Contenido de mi Dieciseisava Publicacion</p>";
        $publicacion->publicado_el = Carbon::now()->subDays(1);
        $publicacion->categoria_id=2;
        $publicacion->facultad_id=2;
        $publicacion->user_id=2;
        $publicacion->estado=1;
        $publicacion->segu_externa ="Privada";
        $publicacion->segu_interna = "Privada";
        $publicacion->save();
    }
}
