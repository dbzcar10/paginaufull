<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPublicaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('url')->unique()->nullable();
            $table->mediumText('extracto')->nullable();
            $table->text('cuerpo')->nullable();
            $table->timestamp('publicado_el')->nullable();
            $table->boolean('estado')->nullanle();
            $table->string('segu_externa')->nullable();
            $table->string('segu_interna')->nullable();
            $table->unsignedInteger('facultad_id')->nullable();
            $table->unsignedInteger('categoria_id')->nullable();
            $table->unsignedInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicaciones');
    }
}
